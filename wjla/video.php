<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WJLA Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WJLA Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wjla/app/">Premium App</a>
| <a href="/wjla/iapp/">iPhone App</a>
| <a href="/wjla/wap/">Mobile Web</a>
| <a href="/wjla/sms/">SMS Usage</a>
| <a href="/wjla/video.php">Video Views</a>
| <a href="/wjla/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">644</td>
	<td align="right">2.22%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">665</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">769</td>
	<td align="right">2.10%</td>
	<td align="right">8</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">778</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">1,153</td>
	<td align="right">3.07%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,163</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">794</td>
	<td align="right">2.55%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">820</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">840</td>
	<td align="right">2.53%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">875</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">796</td>
	<td align="right">2.23%</td>
	<td align="right">100</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">905</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,171</td>
	<td align="right">1.68%</td>
	<td align="right">44</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">1,217</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,427</td>
	<td align="right">2.49%</td>
	<td align="right">79</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">1,511</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,068</td>
	<td align="right">2.74%</td>
	<td align="right">7</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">1,077</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">1,108</td>
	<td align="right">3.12%</td>
	<td align="right">41</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">1,151</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">1,295</td>
	<td align="right">3.56%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,344</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">1,135</td>
	<td align="right">3.38%</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,213</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">902</td>
	<td align="right">2.68%</td>
	<td align="right">123</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,025</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">864</td>
	<td align="right">2.80%</td>
	<td align="right">210</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">1,075</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">798</td>
	<td align="right">2.38%</td>
	<td align="right">181</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">981</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">597</td>
	<td align="right">2.04%</td>
	<td align="right">141</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">738</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">1,067</td>
	<td align="right">4.05%</td>
	<td align="right">144</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,222</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">877</td>
	<td align="right">2.85%</td>
	<td align="right">291</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,170</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">806</td>
	<td align="right">3.05%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">870</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">925</td>
	<td align="right">3.13%</td>
	<td align="right">125</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,050</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,523</td>
	<td align="right">2.50%</td>
	<td align="right">129</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,652</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">1,154</td>
	<td align="right">1.70%</td>
	<td align="right">121</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,275</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,389</td>
	<td align="right">3.96%</td>
	<td align="right">182</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,571</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,521</td>
	<td align="right">4.70%</td>
	<td align="right">115</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,636</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">1,270</td>
	<td align="right">3.94%</td>
	<td align="right">130</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,400</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,257</td>
	<td align="right">3.53%</td>
	<td align="right">98</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,355</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,056</td>
	<td align="right">2.94%</td>
	<td align="right">74</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,130</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,266</td>
	<td align="right">3.56%</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,318</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">909</td>
	<td align="right">3.69%</td>
	<td align="right">110</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,019</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">707</td>
	<td align="right">3.72%</td>
	<td align="right">104</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">811</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">928</td>
	<td align="right">4.57%</td>
	<td align="right">173</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,101</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">821</td>
	<td align="right">3.87%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">890</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">703</td>
	<td align="right">4.32%</td>
	<td align="right">122</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">825</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">416</td>
	<td align="right">4.12%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">448</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">1</td>
	<td align="right">2.33%</td>
	<td align="right">60</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">61</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">36</td>
	<td align="right">6.58%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">79</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">74</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">74</td>
</tr>
<tr>
	<td>Feb. 2006</td>
	<td align="right">0</td>
	<td align="right">0%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">23</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
