<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WCCO Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WCCO Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wcco/app/">Premium App</a>
| <a href="/wcco/iapp/">iPhone App</a>
| <a href="/wcco/wap/">Mobile Web</a>
| <a href="/wcco/sms/">SMS Usage</a>
| <a href="/wcco/video.php">Video Views</a>
| <a href="/wcco/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">188</td>
	<td align="right">0.65%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">194</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">322</td>
	<td align="right">0.88%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">329</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">393</td>
	<td align="right">1.05%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">421</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">410</td>
	<td align="right">1.32%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">421</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">487</td>
	<td align="right">1.47%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">505</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">723</td>
	<td align="right">2.02%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">763</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,578</td>
	<td align="right">2.27%</td>
	<td align="right">58</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,636</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,031</td>
	<td align="right">1.80%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,080</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">750</td>
	<td align="right">1.92%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">775</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">765</td>
	<td align="right">2.15%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">788</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">789</td>
	<td align="right">2.17%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">829</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">632</td>
	<td align="right">1.88%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">645</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">471</td>
	<td align="right">1.40%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">507</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">542</td>
	<td align="right">1.76%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">559</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">677</td>
	<td align="right">2.02%</td>
	<td align="right">42</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">719</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">924</td>
	<td align="right">3.16%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">970</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">551</td>
	<td align="right">2.09%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">581</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">532</td>
	<td align="right">1.73%</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">593</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">577</td>
	<td align="right">2.19%</td>
	<td align="right">101</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">678</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">1,554</td>
	<td align="right">5.26%</td>
	<td align="right">211</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,765</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,182</td>
	<td align="right">1.94%</td>
	<td align="right">149</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,331</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">3,860</td>
	<td align="right">5.69%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,929</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,386</td>
	<td align="right">3.95%</td>
	<td align="right">47</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,433</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">718</td>
	<td align="right">2.22%</td>
	<td align="right">114</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">832</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">647</td>
	<td align="right">2.01%</td>
	<td align="right">147</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">794</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">885</td>
	<td align="right">2.49%</td>
	<td align="right">212</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,097</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">939</td>
	<td align="right">2.61%</td>
	<td align="right">90</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,029</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,102</td>
	<td align="right">3.10%</td>
	<td align="right">135</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,237</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">690</td>
	<td align="right">2.80%</td>
	<td align="right">87</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">777</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">667</td>
	<td align="right">3.51%</td>
	<td align="right">160</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">827</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">549</td>
	<td align="right">2.70%</td>
	<td align="right">137</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">686</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">428</td>
	<td align="right">2.02%</td>
	<td align="right">224</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">652</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">417</td>
	<td align="right">2.56%</td>
	<td align="right">187</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">604</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">168</td>
	<td align="right">1.66%</td>
	<td align="right">158</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">326</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">53</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">13</td>
	<td align="right">2.38%</td>
	<td align="right">37</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">50</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
