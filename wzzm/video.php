<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WZZM Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WZZM Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wzzm/app/">Premium App</a>
| <a href="/wzzm/iapp/">iPhone App</a>
| <a href="/wzzm/wap/">Mobile Web</a>
| <a href="/wzzm/sms/">SMS Usage</a>
| <a href="/wzzm/video.php">Video Views</a>
| <a href="/wzzm/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">96</td>
	<td align="right">0.33%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">103</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">119</td>
	<td align="right">0.32%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">134</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">153</td>
	<td align="right">0.41%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">160</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">191</td>
	<td align="right">0.61%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">195</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">88</td>
	<td align="right">0.27%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">89</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">91</td>
	<td align="right">0.25%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">95</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">295</td>
	<td align="right">0.42%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">301</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">399</td>
	<td align="right">0.70%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">420</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">98</td>
	<td align="right">0.25%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">104</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">102</td>
	<td align="right">0.29%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">103</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">159</td>
	<td align="right">0.44%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">163</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">212</td>
	<td align="right">0.63%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">219</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">98</td>
	<td align="right">0.29%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">104</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">140</td>
	<td align="right">0.45%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">150</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">144</td>
	<td align="right">0.43%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">173</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">174</td>
	<td align="right">0.60%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">200</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">152</td>
	<td align="right">0.58%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">182</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">173</td>
	<td align="right">0.56%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">237</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">223</td>
	<td align="right">0.84%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">266</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">228</td>
	<td align="right">0.77%</td>
	<td align="right">71</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">299</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">807</td>
	<td align="right">1.33%</td>
	<td align="right">75</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">882</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">454</td>
	<td align="right">0.67%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">487</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">348</td>
	<td align="right">0.99%</td>
	<td align="right">113</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">461</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">284</td>
	<td align="right">0.88%</td>
	<td align="right">182</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">466</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">287</td>
	<td align="right">0.89%</td>
	<td align="right">177</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">464</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">470</td>
	<td align="right">1.32%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">556</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">623</td>
	<td align="right">1.73%</td>
	<td align="right">189</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">812</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">682</td>
	<td align="right">1.92%</td>
	<td align="right">117</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">799</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">322</td>
	<td align="right">1.31%</td>
	<td align="right">140</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">462</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">129</td>
	<td align="right">0.68%</td>
	<td align="right">118</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">247</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">322</td>
	<td align="right">1.59%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">360</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">18</td>
	<td align="right">0.08%</td>
	<td align="right">75</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">93</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">2</td>
	<td align="right">0.01%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
