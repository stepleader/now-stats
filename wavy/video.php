<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WAVY Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WAVY Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wavy/app/">Premium App</a>
| <a href="/wavy/iapp/">iPhone App</a>
| <a href="/wavy/wap/">Mobile Web</a>
| <a href="/wavy/sms/">SMS Usage</a>
| <a href="/wavy/video.php">Video Views</a>
| <a href="/wavy/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">304</td>
	<td align="right">1.05%</td>
	<td align="right">0</td>
	<td align="right">70</td>
	<td align="right">0</td>
	<td align="right">374</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">367</td>
	<td align="right">1.00%</td>
	<td align="right">1</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">432</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">334</td>
	<td align="right">0.89%</td>
	<td align="right">4</td>
	<td align="right">114</td>
	<td align="right">0</td>
	<td align="right">452</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">217</td>
	<td align="right">0.70%</td>
	<td align="right">3</td>
	<td align="right">98</td>
	<td align="right">0</td>
	<td align="right">318</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">257</td>
	<td align="right">0.78%</td>
	<td align="right">6</td>
	<td align="right">141</td>
	<td align="right">0</td>
	<td align="right">404</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">222</td>
	<td align="right">0.62%</td>
	<td align="right">15</td>
	<td align="right">302</td>
	<td align="right">0</td>
	<td align="right">539</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">662</td>
	<td align="right">0.95%</td>
	<td align="right">21</td>
	<td align="right">261</td>
	<td align="right">0</td>
	<td align="right">944</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">603</td>
	<td align="right">1.05%</td>
	<td align="right">11</td>
	<td align="right">220</td>
	<td align="right">0</td>
	<td align="right">834</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">288</td>
	<td align="right">0.74%</td>
	<td align="right">4</td>
	<td align="right">204</td>
	<td align="right">0</td>
	<td align="right">496</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">265</td>
	<td align="right">0.75%</td>
	<td align="right">0</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">343</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">218</td>
	<td align="right">0.60%</td>
	<td align="right">10</td>
	<td align="right">73</td>
	<td align="right">0</td>
	<td align="right">301</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">240</td>
	<td align="right">0.71%</td>
	<td align="right">2</td>
	<td align="right">67</td>
	<td align="right">0</td>
	<td align="right">309</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">198</td>
	<td align="right">0.59%</td>
	<td align="right">2</td>
	<td align="right">73</td>
	<td align="right">0</td>
	<td align="right">273</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">169</td>
	<td align="right">0.55%</td>
	<td align="right">0</td>
	<td align="right">88</td>
	<td align="right">0</td>
	<td align="right">258</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">152</td>
	<td align="right">0.45%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">152</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">173</td>
	<td align="right">0.59%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">177</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">137</td>
	<td align="right">0.52%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">159</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">98</td>
	<td align="right">0.32%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">155</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">22</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
