<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KTVF Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KTVF Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/ktvf/app/">Premium App</a>
| <a href="/ktvf/iapp/">iPhone App</a>
| <a href="/ktvf/wap/">Mobile Web</a>
| <a href="/ktvf/sms/">SMS Usage</a>
| <a href="/ktvf/video.php">Video Views</a>
| <a href="/ktvf/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">170</td>
	<td align="right">0.59%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">170</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">292</td>
	<td align="right">0.80%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">294</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">442</td>
	<td align="right">1.18%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">442</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">402</td>
	<td align="right">1.29%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">402</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">353</td>
	<td align="right">1.06%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">354</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">187</td>
	<td align="right">0.52%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">203</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
