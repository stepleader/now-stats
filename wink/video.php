<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WINK Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WINK Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wink/app/">Premium App</a>
| <a href="/wink/iapp/">iPhone App</a>
| <a href="/wink/wap/">Mobile Web</a>
| <a href="/wink/sms/">SMS Usage</a>
| <a href="/wink/video.php">Video Views</a>
| <a href="/wink/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">212</td>
	<td align="right">0.73%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">221</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">311</td>
	<td align="right">0.85%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">314</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">421</td>
	<td align="right">1.12%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">431</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">77</td>
	<td align="right">0.25%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">77</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">36</td>
	<td align="right">0.11%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">52</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">204</td>
	<td align="right">0.57%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">205</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">494</td>
	<td align="right">0.71%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">512</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">808</td>
	<td align="right">1.41%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">852</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">214</td>
	<td align="right">0.55%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">215</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">283</td>
	<td align="right">0.80%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">294</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">204</td>
	<td align="right">0.56%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">206</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">257</td>
	<td align="right">0.76%</td>
	<td align="right">55</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">312</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">276</td>
	<td align="right">0.82%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">311</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">287</td>
	<td align="right">0.93%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">315</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">258</td>
	<td align="right">0.77%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">315</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">340</td>
	<td align="right">1.16%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">348</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">268</td>
	<td align="right">1.02%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">270</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">341</td>
	<td align="right">1.11%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">349</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">471</td>
	<td align="right">1.78%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">472</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">209</td>
	<td align="right">0.71%</td>
	<td align="right">154</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">363</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">33</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
