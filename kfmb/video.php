<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KFMB Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KFMB Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kfmb/app/">Premium App</a>
| <a href="/kfmb/iapp/">iPhone App</a>
| <a href="/kfmb/wap/">Mobile Web</a>
| <a href="/kfmb/sms/">SMS Usage</a>
| <a href="/kfmb/video.php">Video Views</a>
| <a href="/kfmb/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">386</td>
	<td align="right">1.33%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">394</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">560</td>
	<td align="right">1.53%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">571</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">264</td>
	<td align="right">0.70%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">271</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">419</td>
	<td align="right">1.35%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">427</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">344</td>
	<td align="right">1.04%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">351</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">401</td>
	<td align="right">1.12%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">415</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">536</td>
	<td align="right">0.77%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">545</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">570</td>
	<td align="right">0.99%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">586</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">464</td>
	<td align="right">1.19%</td>
	<td align="right">34</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">543</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">360</td>
	<td align="right">1.01%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">444</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">375</td>
	<td align="right">1.03%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">398</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">426</td>
	<td align="right">1.27%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">427</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">375</td>
	<td align="right">1.12%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">387</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">281</td>
	<td align="right">0.91%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">286</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">365</td>
	<td align="right">1.09%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">392</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">249</td>
	<td align="right">0.85%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">258</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">306</td>
	<td align="right">1.16%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">334</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">676</td>
	<td align="right">2.19%</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">728</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">15</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
