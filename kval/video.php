<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KVAL Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KVAL Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kval/app/">Premium App</a>
| <a href="/kval/iapp/">iPhone App</a>
| <a href="/kval/wap/">Mobile Web</a>
| <a href="/kval/sms/">SMS Usage</a>
| <a href="/kval/video.php">Video Views</a>
| <a href="/kval/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">136</td>
	<td align="right">0.47%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">136</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">179</td>
	<td align="right">0.49%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">179</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">169</td>
	<td align="right">0.45%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">169</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">70</td>
	<td align="right">0.23%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">73</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">51</td>
	<td align="right">0.15%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">51</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">75</td>
	<td align="right">0.21%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">76</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">212</td>
	<td align="right">0.30%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">213</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">203</td>
	<td align="right">0.35%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">218</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">27</td>
	<td align="right">0.07%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">50</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">27</td>
	<td align="right">0.08%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">27</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">150</td>
	<td align="right">0.41%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">157</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">103</td>
	<td align="right">0.31%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">103</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">41</td>
	<td align="right">0.12%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">41</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">38</td>
	<td align="right">0.12%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">38</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">145</td>
	<td align="right">0.43%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">148</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">66</td>
	<td align="right">0.23%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">67</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">48</td>
	<td align="right">0.18%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">62</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">46</td>
	<td align="right">0.15%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">60</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">39</td>
	<td align="right">0.15%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">39</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">54</td>
	<td align="right">0.18%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">73</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">722</td>
	<td align="right">1.19%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">737</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">7</td>
	<td align="right">0.01%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">53</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
