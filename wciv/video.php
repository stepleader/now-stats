<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WCIV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WCIV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wciv/app/">Premium App</a>
| <a href="/wciv/iapp/">iPhone App</a>
| <a href="/wciv/wap/">Mobile Web</a>
| <a href="/wciv/sms/">SMS Usage</a>
| <a href="/wciv/video.php">Video Views</a>
| <a href="/wciv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">81</td>
	<td align="right">0.28%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">83</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">113</td>
	<td align="right">0.31%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">113</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">134</td>
	<td align="right">0.36%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">135</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">112</td>
	<td align="right">0.36%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">112</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">128</td>
	<td align="right">0.39%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">129</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">88</td>
	<td align="right">0.25%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">100</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">365</td>
	<td align="right">0.52%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">368</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">239</td>
	<td align="right">0.42%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">244</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">150</td>
	<td align="right">0.38%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">154</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">157</td>
	<td align="right">0.44%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">159</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">201</td>
	<td align="right">0.55%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">202</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">214</td>
	<td align="right">0.64%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">215</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">145</td>
	<td align="right">0.43%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">151</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">111</td>
	<td align="right">0.36%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">111</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">120</td>
	<td align="right">0.36%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">122</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">88</td>
	<td align="right">0.30%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">90</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">98</td>
	<td align="right">0.37%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">98</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">170</td>
	<td align="right">0.55%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">184</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">197</td>
	<td align="right">0.75%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">202</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">134</td>
	<td align="right">0.45%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">137</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">641</td>
	<td align="right">1.05%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">641</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">435</td>
	<td align="right">0.64%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">448</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">261</td>
	<td align="right">0.74%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">262</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">176</td>
	<td align="right">0.54%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">177</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">280</td>
	<td align="right">0.87%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">294</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">297</td>
	<td align="right">0.83%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">315</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
