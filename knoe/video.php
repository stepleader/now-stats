<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KNOE Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KNOE Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/knoe/app/">Premium App</a>
| <a href="/knoe/iapp/">iPhone App</a>
| <a href="/knoe/wap/">Mobile Web</a>
| <a href="/knoe/sms/">SMS Usage</a>
| <a href="/knoe/video.php">Video Views</a>
| <a href="/knoe/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">33</td>
	<td align="right">0.11%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">33</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">72</td>
	<td align="right">0.20%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">73</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">58</td>
	<td align="right">0.15%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">58</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">57</td>
	<td align="right">0.18%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">75</td>
	<td align="right">0.23%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">75</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">85</td>
	<td align="right">0.24%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">86</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">354</td>
	<td align="right">0.51%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">354</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">350</td>
	<td align="right">0.61%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">355</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">63</td>
	<td align="right">0.16%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">63</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">73</td>
	<td align="right">0.21%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">75</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">98</td>
	<td align="right">0.27%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">98</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">129</td>
	<td align="right">0.38%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">132</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">158</td>
	<td align="right">0.47%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">162</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">127</td>
	<td align="right">0.41%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">129</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">121</td>
	<td align="right">0.36%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">145</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">22</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
