<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WFSB Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WFSB Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wfsb/app/">Premium App</a>
| <a href="/wfsb/iapp/">iPhone App</a>
| <a href="/wfsb/wap/">Mobile Web</a>
| <a href="/wfsb/sms/">SMS Usage</a>
| <a href="/wfsb/video.php">Video Views</a>
| <a href="/wfsb/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">223</td>
	<td align="right">0.77%</td>
	<td align="right">90</td>
	<td align="right">519</td>
	<td align="right">0</td>
	<td align="right">832</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">398</td>
	<td align="right">1.08%</td>
	<td align="right">4</td>
	<td align="right">574</td>
	<td align="right">0</td>
	<td align="right">976</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">345</td>
	<td align="right">0.92%</td>
	<td align="right">6</td>
	<td align="right">907</td>
	<td align="right">0</td>
	<td align="right">1,258</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">252</td>
	<td align="right">0.81%</td>
	<td align="right">8</td>
	<td align="right">800</td>
	<td align="right">0</td>
	<td align="right">1,060</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">248</td>
	<td align="right">0.75%</td>
	<td align="right">13</td>
	<td align="right">259</td>
	<td align="right">0</td>
	<td align="right">520</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">294</td>
	<td align="right">0.82%</td>
	<td align="right">9</td>
	<td align="right">283</td>
	<td align="right">0</td>
	<td align="right">586</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">513</td>
	<td align="right">0.74%</td>
	<td align="right">16</td>
	<td align="right">289</td>
	<td align="right">0</td>
	<td align="right">818</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">610</td>
	<td align="right">1.06%</td>
	<td align="right">44</td>
	<td align="right">261</td>
	<td align="right">0</td>
	<td align="right">915</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">425</td>
	<td align="right">1.09%</td>
	<td align="right">12</td>
	<td align="right">270</td>
	<td align="right">0</td>
	<td align="right">713</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">357</td>
	<td align="right">1.00%</td>
	<td align="right">38</td>
	<td align="right">484</td>
	<td align="right">0</td>
	<td align="right">879</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">421</td>
	<td align="right">1.16%</td>
	<td align="right">85</td>
	<td align="right">199</td>
	<td align="right">0</td>
	<td align="right">705</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">389</td>
	<td align="right">1.16%</td>
	<td align="right">164</td>
	<td align="right">133</td>
	<td align="right">0</td>
	<td align="right">686</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">391</td>
	<td align="right">1.16%</td>
	<td align="right">37</td>
	<td align="right">167</td>
	<td align="right">0</td>
	<td align="right">595</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">498</td>
	<td align="right">1.61%</td>
	<td align="right">6</td>
	<td align="right">645</td>
	<td align="right">0</td>
	<td align="right">1,149</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">357</td>
	<td align="right">1.06%</td>
	<td align="right">25</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">421</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">366</td>
	<td align="right">1.25%</td>
	<td align="right">13</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">380</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">320</td>
	<td align="right">1.21%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">325</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">429</td>
	<td align="right">1.39%</td>
	<td align="right">17</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">459</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">274</td>
	<td align="right">1.04%</td>
	<td align="right">25</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">300</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">487</td>
	<td align="right">1.65%</td>
	<td align="right">38</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">526</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,253</td>
	<td align="right">2.06%</td>
	<td align="right">28</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">1,283</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">708</td>
	<td align="right">1.04%</td>
	<td align="right">37</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">745</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">730</td>
	<td align="right">2.08%</td>
	<td align="right">91</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">821</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">650</td>
	<td align="right">2.01%</td>
	<td align="right">88</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">738</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">587</td>
	<td align="right">1.82%</td>
	<td align="right">60</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">647</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">792</td>
	<td align="right">2.22%</td>
	<td align="right">134</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">926</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">843</td>
	<td align="right">2.35%</td>
	<td align="right">132</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">975</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">710</td>
	<td align="right">2.00%</td>
	<td align="right">236</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">946</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">273</td>
	<td align="right">1.11%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">342</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">238</td>
	<td align="right">1.25%</td>
	<td align="right">94</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">332</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">129</td>
	<td align="right">0.64%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">180</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
