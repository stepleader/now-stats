<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KTVT Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KTVT Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/ktvt/app/">Premium App</a>
| <a href="/ktvt/iapp/">iPhone App</a>
| <a href="/ktvt/wap/">Mobile Web</a>
| <a href="/ktvt/sms/">SMS Usage</a>
| <a href="/ktvt/video.php">Video Views</a>
| <a href="/ktvt/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">336</td>
	<td align="right">1.16%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">348</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">544</td>
	<td align="right">1.48%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">562</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">832</td>
	<td align="right">2.21%</td>
	<td align="right">37</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">869</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">702</td>
	<td align="right">2.26%</td>
	<td align="right">59</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">761</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">680</td>
	<td align="right">2.05%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">701</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">833</td>
	<td align="right">2.33%</td>
	<td align="right">69</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">903</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">4,936</td>
	<td align="right">7.10%</td>
	<td align="right">43</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">4,980</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,522</td>
	<td align="right">2.65%</td>
	<td align="right">110</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">1,633</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,467</td>
	<td align="right">3.76%</td>
	<td align="right">42</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,509</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">1,266</td>
	<td align="right">3.56%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,292</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">1,201</td>
	<td align="right">3.30%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,220</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">1,476</td>
	<td align="right">4.39%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,525</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">1,372</td>
	<td align="right">4.08%</td>
	<td align="right">77</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,449</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">1,001</td>
	<td align="right">3.24%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,022</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">1,203</td>
	<td align="right">3.59%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,260</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">923</td>
	<td align="right">3.16%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">986</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">975</td>
	<td align="right">3.70%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,013</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">1,486</td>
	<td align="right">4.82%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,531</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">1,104</td>
	<td align="right">4.18%</td>
	<td align="right">66</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,170</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">1,556</td>
	<td align="right">5.26%</td>
	<td align="right">273</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,829</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">3,140</td>
	<td align="right">5.16%</td>
	<td align="right">288</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,428</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">2,340</td>
	<td align="right">3.45%</td>
	<td align="right">168</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,508</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">2,595</td>
	<td align="right">7.39%</td>
	<td align="right">265</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,860</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,892</td>
	<td align="right">5.84%</td>
	<td align="right">210</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,102</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">1,505</td>
	<td align="right">4.67%</td>
	<td align="right">151</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,656</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,149</td>
	<td align="right">3.23%</td>
	<td align="right">114</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,263</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,310</td>
	<td align="right">3.65%</td>
	<td align="right">238</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,548</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,389</td>
	<td align="right">3.91%</td>
	<td align="right">102</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,491</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">1,071</td>
	<td align="right">4.35%</td>
	<td align="right">175</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,246</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">1,018</td>
	<td align="right">5.36%</td>
	<td align="right">145</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,163</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">1,175</td>
	<td align="right">5.78%</td>
	<td align="right">139</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,314</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">1,220</td>
	<td align="right">5.74%</td>
	<td align="right">205</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,425</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">962</td>
	<td align="right">5.91%</td>
	<td align="right">163</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,125</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">624</td>
	<td align="right">6.18%</td>
	<td align="right">157</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">781</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">2</td>
	<td align="right">4.65%</td>
	<td align="right">144</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">146</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">12</td>
	<td align="right">2.19%</td>
	<td align="right">144</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">156</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">41</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">41</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
