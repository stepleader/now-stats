<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WDTN Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WDTN Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wdtn/app/">Premium App</a>
| <a href="/wdtn/iapp/">iPhone App</a>
| <a href="/wdtn/wap/">Mobile Web</a>
| <a href="/wdtn/sms/">SMS Usage</a>
| <a href="/wdtn/video.php">Video Views</a>
| <a href="/wdtn/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">58</td>
	<td align="right">0.20%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">58</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">93</td>
	<td align="right">0.25%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">99</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">165</td>
	<td align="right">0.44%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">168</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">180</td>
	<td align="right">0.58%</td>
	<td align="right">12</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">195</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">143</td>
	<td align="right">0.43%</td>
	<td align="right">19</td>
	<td align="right">42</td>
	<td align="right">0</td>
	<td align="right">204</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">191</td>
	<td align="right">0.53%</td>
	<td align="right">40</td>
	<td align="right">73</td>
	<td align="right">0</td>
	<td align="right">304</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">491</td>
	<td align="right">0.71%</td>
	<td align="right">41</td>
	<td align="right">139</td>
	<td align="right">0</td>
	<td align="right">671</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">387</td>
	<td align="right">0.67%</td>
	<td align="right">44</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">482</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">291</td>
	<td align="right">0.75%</td>
	<td align="right">2</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">338</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">276</td>
	<td align="right">0.78%</td>
	<td align="right">0</td>
	<td align="right">73</td>
	<td align="right">0</td>
	<td align="right">349</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">261</td>
	<td align="right">0.72%</td>
	<td align="right">1</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">315</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">220</td>
	<td align="right">0.65%</td>
	<td align="right">8</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">289</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">245</td>
	<td align="right">0.73%</td>
	<td align="right">5</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">302</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">207</td>
	<td align="right">0.67%</td>
	<td align="right">7</td>
	<td align="right">65</td>
	<td align="right">0</td>
	<td align="right">279</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">178</td>
	<td align="right">0.53%</td>
	<td align="right">3</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">182</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">214</td>
	<td align="right">0.73%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">220</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">184</td>
	<td align="right">0.70%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">190</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">140</td>
	<td align="right">0.45%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">184</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
