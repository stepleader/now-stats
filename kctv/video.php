<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KCTV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KCTV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kctv/app/">Premium App</a>
| <a href="/kctv/iapp/">iPhone App</a>
| <a href="/kctv/wap/">Mobile Web</a>
| <a href="/kctv/sms/">SMS Usage</a>
| <a href="/kctv/video.php">Video Views</a>
| <a href="/kctv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">324</td>
	<td align="right">1.12%</td>
	<td align="right">14</td>
	<td align="right">355</td>
	<td align="right">0</td>
	<td align="right">693</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">506</td>
	<td align="right">1.38%</td>
	<td align="right">47</td>
	<td align="right">432</td>
	<td align="right">0</td>
	<td align="right">985</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">535</td>
	<td align="right">1.42%</td>
	<td align="right">30</td>
	<td align="right">482</td>
	<td align="right">0</td>
	<td align="right">1,047</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">591</td>
	<td align="right">1.90%</td>
	<td align="right">38</td>
	<td align="right">1,024</td>
	<td align="right">0</td>
	<td align="right">1,653</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">377</td>
	<td align="right">1.14%</td>
	<td align="right">25</td>
	<td align="right">304</td>
	<td align="right">0</td>
	<td align="right">706</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">602</td>
	<td align="right">1.68%</td>
	<td align="right">60</td>
	<td align="right">285</td>
	<td align="right">0</td>
	<td align="right">947</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">722</td>
	<td align="right">1.04%</td>
	<td align="right">74</td>
	<td align="right">330</td>
	<td align="right">0</td>
	<td align="right">1,126</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">882</td>
	<td align="right">1.54%</td>
	<td align="right">121</td>
	<td align="right">258</td>
	<td align="right">0</td>
	<td align="right">1,261</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">957</td>
	<td align="right">2.45%</td>
	<td align="right">39</td>
	<td align="right">524</td>
	<td align="right">0</td>
	<td align="right">1,520</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">992</td>
	<td align="right">2.79%</td>
	<td align="right">81</td>
	<td align="right">730</td>
	<td align="right">0</td>
	<td align="right">1,803</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">965</td>
	<td align="right">2.65%</td>
	<td align="right">72</td>
	<td align="right">454</td>
	<td align="right">0</td>
	<td align="right">1,491</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">583</td>
	<td align="right">1.73%</td>
	<td align="right">143</td>
	<td align="right">201</td>
	<td align="right">0</td>
	<td align="right">927</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">675</td>
	<td align="right">2.01%</td>
	<td align="right">192</td>
	<td align="right">138</td>
	<td align="right">0</td>
	<td align="right">1,005</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">657</td>
	<td align="right">2.13%</td>
	<td align="right">52</td>
	<td align="right">285</td>
	<td align="right">0</td>
	<td align="right">994</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">740</td>
	<td align="right">2.21%</td>
	<td align="right">38</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">817</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">718</td>
	<td align="right">2.46%</td>
	<td align="right">87</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">806</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">570</td>
	<td align="right">2.16%</td>
	<td align="right">110</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">680</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">663</td>
	<td align="right">2.15%</td>
	<td align="right">119</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">782</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">553</td>
	<td align="right">2.09%</td>
	<td align="right">152</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">706</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">591</td>
	<td align="right">2.00%</td>
	<td align="right">123</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">715</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,398</td>
	<td align="right">2.30%</td>
	<td align="right">90</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">1,491</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">1,319</td>
	<td align="right">1.94%</td>
	<td align="right">115</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,434</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,337</td>
	<td align="right">3.81%</td>
	<td align="right">134</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,471</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,139</td>
	<td align="right">3.52%</td>
	<td align="right">135</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,274</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">947</td>
	<td align="right">2.94%</td>
	<td align="right">96</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,043</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">951</td>
	<td align="right">2.67%</td>
	<td align="right">137</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,088</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,321</td>
	<td align="right">3.68%</td>
	<td align="right">136</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,457</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,421</td>
	<td align="right">4.00%</td>
	<td align="right">67</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,488</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">848</td>
	<td align="right">3.45%</td>
	<td align="right">171</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,019</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">614</td>
	<td align="right">3.23%</td>
	<td align="right">139</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">753</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">698</td>
	<td align="right">3.44%</td>
	<td align="right">152</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">850</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">781</td>
	<td align="right">3.68%</td>
	<td align="right">141</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">922</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">732</td>
	<td align="right">4.50%</td>
	<td align="right">105</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">837</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">417</td>
	<td align="right">4.13%</td>
	<td align="right">122</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">539</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">48</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
