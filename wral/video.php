<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WRAL Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WRAL Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wral/app/">Premium App</a>
| <a href="/wral/iapp/">iPhone App</a>
| <a href="/wral/wap/">Mobile Web</a>
| <a href="/wral/sms/">SMS Usage</a>
| <a href="/wral/video.php">Video Views</a>
| <a href="/wral/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">230</td>
	<td align="right">0.79%</td>
	<td align="right">18</td>
	<td align="right">2,506</td>
	<td align="right">585</td>
	<td align="right">3,344</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">262</td>
	<td align="right">0.71%</td>
	<td align="right">11</td>
	<td align="right">1,816</td>
	<td align="right">153</td>
	<td align="right">2,242</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">318</td>
	<td align="right">0.85%</td>
	<td align="right">13</td>
	<td align="right">2,067</td>
	<td align="right">608</td>
	<td align="right">3,006</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">366</td>
	<td align="right">1.18%</td>
	<td align="right">26</td>
	<td align="right">2,277</td>
	<td align="right">168</td>
	<td align="right">2,837</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">513</td>
	<td align="right">1.55%</td>
	<td align="right">42</td>
	<td align="right">1,263</td>
	<td align="right">516</td>
	<td align="right">2,334</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">467</td>
	<td align="right">1.31%</td>
	<td align="right">106</td>
	<td align="right">1,144</td>
	<td align="right">123</td>
	<td align="right">1,844</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">996</td>
	<td align="right">1.43%</td>
	<td align="right">117</td>
	<td align="right">1,259</td>
	<td align="right">584</td>
	<td align="right">2,956</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">827</td>
	<td align="right">1.44%</td>
	<td align="right">129</td>
	<td align="right">976</td>
	<td align="right">82</td>
	<td align="right">2,014</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">537</td>
	<td align="right">1.38%</td>
	<td align="right">113</td>
	<td align="right">1,055</td>
	<td align="right">136</td>
	<td align="right">1,841</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">361</td>
	<td align="right">1.02%</td>
	<td align="right">22</td>
	<td align="right">819</td>
	<td align="right">72</td>
	<td align="right">1,278</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">543</td>
	<td align="right">1.49%</td>
	<td align="right">58</td>
	<td align="right">861</td>
	<td align="right">67</td>
	<td align="right">1,530</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">507</td>
	<td align="right">1.51%</td>
	<td align="right">48</td>
	<td align="right">684</td>
	<td align="right">113</td>
	<td align="right">1,352</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">537</td>
	<td align="right">1.60%</td>
	<td align="right">50</td>
	<td align="right">828</td>
	<td align="right">144</td>
	<td align="right">1,561</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">416</td>
	<td align="right">1.35%</td>
	<td align="right">38</td>
	<td align="right">445</td>
	<td align="right">86</td>
	<td align="right">993</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">497</td>
	<td align="right">1.48%</td>
	<td align="right">109</td>
	<td align="right">547</td>
	<td align="right">144</td>
	<td align="right">1,319</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">430</td>
	<td align="right">1.47%</td>
	<td align="right">87</td>
	<td align="right">414</td>
	<td align="right">304</td>
	<td align="right">1,235</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">374</td>
	<td align="right">1.42%</td>
	<td align="right">142</td>
	<td align="right">299</td>
	<td align="right">91</td>
	<td align="right">945</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">518</td>
	<td align="right">1.68%</td>
	<td align="right">131</td>
	<td align="right">317</td>
	<td align="right">0</td>
	<td align="right">1,028</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">353</td>
	<td align="right">1.34%</td>
	<td align="right">185</td>
	<td align="right">339</td>
	<td align="right">0</td>
	<td align="right">902</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">380</td>
	<td align="right">1.29%</td>
	<td align="right">280</td>
	<td align="right">156</td>
	<td align="right">0</td>
	<td align="right">827</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">939</td>
	<td align="right">1.54%</td>
	<td align="right">269</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">1,229</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">1,973</td>
	<td align="right">2.91%</td>
	<td align="right">141</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,114</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">558</td>
	<td align="right">1.59%</td>
	<td align="right">186</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">744</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">698</td>
	<td align="right">2.15%</td>
	<td align="right">196</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">894</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">706</td>
	<td align="right">2.19%</td>
	<td align="right">289</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">995</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">818</td>
	<td align="right">2.30%</td>
	<td align="right">306</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,124</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,001</td>
	<td align="right">2.79%</td>
	<td align="right">180</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,181</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">969</td>
	<td align="right">2.73%</td>
	<td align="right">111</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,080</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">486</td>
	<td align="right">1.98%</td>
	<td align="right">207</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">693</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">286</td>
	<td align="right">1.50%</td>
	<td align="right">271</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">557</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">387</td>
	<td align="right">1.91%</td>
	<td align="right">232</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">619</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">448</td>
	<td align="right">2.11%</td>
	<td align="right">171</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">619</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">494</td>
	<td align="right">3.04%</td>
	<td align="right">286</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">780</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">389</td>
	<td align="right">3.85%</td>
	<td align="right">254</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">643</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">27</td>
	<td align="right">62.79%</td>
	<td align="right">207</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">234</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">124</td>
	<td align="right">22.67%</td>
	<td align="right">411</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">535</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">3</td>
	<td align="right">100.00%</td>
	<td align="right">346</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">349</td>
</tr>
<tr>
	<td>Feb. 2006</td>
	<td align="right">0</td>
	<td align="right">0%</td>
	<td align="right">382</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">382</td>
</tr>
<tr>
	<td>Jan. 2006</td>
	<td align="right">0</td>
	<td align="right">0%</td>
	<td align="right">397</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">397</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
