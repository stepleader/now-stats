<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WBBM Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WBBM Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wbbm/app/">Premium App</a>
| <a href="/wbbm/iapp/">iPhone App</a>
| <a href="/wbbm/wap/">Mobile Web</a>
| <a href="/wbbm/sms/">SMS Usage</a>
| <a href="/wbbm/video.php">Video Views</a>
| <a href="/wbbm/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">456</td>
	<td align="right">1.57%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">465</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">779</td>
	<td align="right">2.12%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">803</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">1,419</td>
	<td align="right">3.77%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,451</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">1,286</td>
	<td align="right">4.14%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,313</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">1,765</td>
	<td align="right">5.32%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,786</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">1,689</td>
	<td align="right">4.72%</td>
	<td align="right">67</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,768</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,610</td>
	<td align="right">2.32%</td>
	<td align="right">72</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,682</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">2,069</td>
	<td align="right">3.61%</td>
	<td align="right">173</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,242</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">2,317</td>
	<td align="right">5.93%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,354</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">2,626</td>
	<td align="right">7.39%</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,664</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">2,030</td>
	<td align="right">5.58%</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,082</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">1,470</td>
	<td align="right">4.37%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,513</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">1,248</td>
	<td align="right">3.71%</td>
	<td align="right">73</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,321</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">1,629</td>
	<td align="right">5.28%</td>
	<td align="right">118</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,747</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">1,413</td>
	<td align="right">4.21%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,477</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">1,512</td>
	<td align="right">5.18%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,550</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">1,298</td>
	<td align="right">4.93%</td>
	<td align="right">58</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,356</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">1,290</td>
	<td align="right">4.19%</td>
	<td align="right">136</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,426</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">1,419</td>
	<td align="right">5.38%</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,458</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">1,586</td>
	<td align="right">5.37%</td>
	<td align="right">87</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,675</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,922</td>
	<td align="right">3.16%</td>
	<td align="right">90</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,012</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">1,780</td>
	<td align="right">2.62%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,866</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,372</td>
	<td align="right">3.91%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,423</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,423</td>
	<td align="right">4.39%</td>
	<td align="right">91</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,514</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">1,661</td>
	<td align="right">5.15%</td>
	<td align="right">164</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,825</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,686</td>
	<td align="right">4.73%</td>
	<td align="right">231</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,917</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,821</td>
	<td align="right">5.07%</td>
	<td align="right">193</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,014</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">2,387</td>
	<td align="right">6.71%</td>
	<td align="right">158</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,545</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">2,121</td>
	<td align="right">8.62%</td>
	<td align="right">303</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,424</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">1,911</td>
	<td align="right">10.05%</td>
	<td align="right">403</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,314</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">1,728</td>
	<td align="right">8.51%</td>
	<td align="right">301</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,029</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">1,683</td>
	<td align="right">7.92%</td>
	<td align="right">370</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,053</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">1,219</td>
	<td align="right">7.49%</td>
	<td align="right">302</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,521</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">878</td>
	<td align="right">8.69%</td>
	<td align="right">274</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,152</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">179</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">179</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">21</td>
	<td align="right">3.84%</td>
	<td align="right">164</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">185</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">24</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
