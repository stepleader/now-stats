<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KOVR Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KOVR Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kovr/app/">Premium App</a>
| <a href="/kovr/iapp/">iPhone App</a>
| <a href="/kovr/wap/">Mobile Web</a>
| <a href="/kovr/sms/">SMS Usage</a>
| <a href="/kovr/video.php">Video Views</a>
| <a href="/kovr/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">397</td>
	<td align="right">1.37%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">400</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">682</td>
	<td align="right">1.86%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">685</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">671</td>
	<td align="right">1.78%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">684</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">736</td>
	<td align="right">2.37%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">742</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">546</td>
	<td align="right">1.65%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">576</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">481</td>
	<td align="right">1.34%</td>
	<td align="right">174</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">655</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">854</td>
	<td align="right">1.23%</td>
	<td align="right">189</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,043</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,118</td>
	<td align="right">1.95%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,146</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,111</td>
	<td align="right">2.85%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,112</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">1,065</td>
	<td align="right">3.00%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,074</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">661</td>
	<td align="right">1.82%</td>
	<td align="right">90</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">751</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">519</td>
	<td align="right">1.54%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">535</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">495</td>
	<td align="right">1.47%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">541</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">488</td>
	<td align="right">1.58%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">497</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">883</td>
	<td align="right">2.63%</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">922</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">659</td>
	<td align="right">2.26%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">670</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">377</td>
	<td align="right">1.43%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">381</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">563</td>
	<td align="right">1.83%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">577</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">631</td>
	<td align="right">2.39%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">655</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">766</td>
	<td align="right">2.59%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">795</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,225</td>
	<td align="right">2.01%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,254</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">681</td>
	<td align="right">1.00%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">691</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">755</td>
	<td align="right">2.15%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">770</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">861</td>
	<td align="right">2.66%</td>
	<td align="right">65</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">926</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">794</td>
	<td align="right">2.46%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">838</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">897</td>
	<td align="right">2.52%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">945</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,153</td>
	<td align="right">3.21%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,201</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,200</td>
	<td align="right">3.38%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,236</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">839</td>
	<td align="right">3.41%</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">900</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">612</td>
	<td align="right">3.22%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">657</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">550</td>
	<td align="right">2.71%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">614</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">777</td>
	<td align="right">3.66%</td>
	<td align="right">127</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">904</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">510</td>
	<td align="right">3.14%</td>
	<td align="right">125</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">635</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">197</td>
	<td align="right">1.95%</td>
	<td align="right">60</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">257</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">34</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">30</td>
	<td align="right">5.48%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">43</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
