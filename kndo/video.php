<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KNDO Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KNDO Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kndo/app/">Premium App</a>
| <a href="/kndo/iapp/">iPhone App</a>
| <a href="/kndo/wap/">Mobile Web</a>
| <a href="/kndo/sms/">SMS Usage</a>
| <a href="/kndo/video.php">Video Views</a>
| <a href="/kndo/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">62</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">62</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">49</td>
	<td align="right">0.13%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">49</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">91</td>
	<td align="right">0.24%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">94</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">53</td>
	<td align="right">0.17%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">54</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">34</td>
	<td align="right">0.10%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">34</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">113</td>
	<td align="right">0.32%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">115</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">237</td>
	<td align="right">0.34%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">237</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">242</td>
	<td align="right">0.42%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">242</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">116</td>
	<td align="right">0.30%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">116</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">117</td>
	<td align="right">0.33%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">117</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">80</td>
	<td align="right">0.22%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">80</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">96</td>
	<td align="right">0.29%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">96</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">154</td>
	<td align="right">0.46%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">154</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">74</td>
	<td align="right">0.24%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">74</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">78</td>
	<td align="right">0.23%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">80</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">92</td>
	<td align="right">0.32%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">92</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">50</td>
	<td align="right">0.19%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">51</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">78</td>
	<td align="right">0.25%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">80</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">57</td>
	<td align="right">0.22%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">88</td>
	<td align="right">0.30%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">88</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">784</td>
	<td align="right">1.29%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">784</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">285</td>
	<td align="right">0.42%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">285</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">194</td>
	<td align="right">0.55%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">194</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">218</td>
	<td align="right">0.67%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">228</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">248</td>
	<td align="right">0.77%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">280</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">35</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
