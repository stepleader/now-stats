<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WFTS Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WFTS Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wfts/app/">Premium App</a>
| <a href="/wfts/iapp/">iPhone App</a>
| <a href="/wfts/wap/">Mobile Web</a>
| <a href="/wfts/sms/">SMS Usage</a>
| <a href="/wfts/video.php">Video Views</a>
| <a href="/wfts/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">216</td>
	<td align="right">0.74%</td>
	<td align="right">6</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">300</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">336</td>
	<td align="right">0.92%</td>
	<td align="right">7</td>
	<td align="right">108</td>
	<td align="right">0</td>
	<td align="right">451</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">279</td>
	<td align="right">0.74%</td>
	<td align="right">3</td>
	<td align="right">99</td>
	<td align="right">0</td>
	<td align="right">381</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">264</td>
	<td align="right">0.85%</td>
	<td align="right">6</td>
	<td align="right">65</td>
	<td align="right">0</td>
	<td align="right">335</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">293</td>
	<td align="right">0.88%</td>
	<td align="right">12</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">343</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">421</td>
	<td align="right">1.18%</td>
	<td align="right">17</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">482</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">882</td>
	<td align="right">1.27%</td>
	<td align="right">28</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">946</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">282</td>
	<td align="right">0.49%</td>
	<td align="right">10</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">330</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
