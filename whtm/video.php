<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WHTM Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WHTM Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/whtm/app/">Premium App</a>
| <a href="/whtm/iapp/">iPhone App</a>
| <a href="/whtm/wap/">Mobile Web</a>
| <a href="/whtm/sms/">SMS Usage</a>
| <a href="/whtm/video.php">Video Views</a>
| <a href="/whtm/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">50</td>
	<td align="right">0.17%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">50</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">102</td>
	<td align="right">0.28%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">104</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">96</td>
	<td align="right">0.26%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">98</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">162</td>
	<td align="right">0.52%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">163</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">140</td>
	<td align="right">0.42%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">140</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">77</td>
	<td align="right">0.22%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">78</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">335</td>
	<td align="right">0.48%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">344</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">363</td>
	<td align="right">0.63%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">364</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">89</td>
	<td align="right">0.23%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">95</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">51</td>
	<td align="right">0.14%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">51</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">80</td>
	<td align="right">0.22%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">95</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">124</td>
	<td align="right">0.37%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">126</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">215</td>
	<td align="right">0.64%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">215</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">138</td>
	<td align="right">0.45%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">141</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">99</td>
	<td align="right">0.30%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">103</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">87</td>
	<td align="right">0.30%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">96</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">130</td>
	<td align="right">0.49%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">147</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">139</td>
	<td align="right">0.45%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">152</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">187</td>
	<td align="right">0.71%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">188</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">430</td>
	<td align="right">1.45%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">432</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">928</td>
	<td align="right">1.52%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">944</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">6,544</td>
	<td align="right">9.64%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">6,549</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">157</td>
	<td align="right">0.45%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">160</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">123</td>
	<td align="right">0.38%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">156</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">180</td>
	<td align="right">0.56%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">180</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">254</td>
	<td align="right">0.71%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">276</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
