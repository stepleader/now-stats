<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WBOC Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WBOC Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wboc/app/">Premium App</a>
| <a href="/wboc/iapp/">iPhone App</a>
| <a href="/wboc/wap/">Mobile Web</a>
| <a href="/wboc/sms/">SMS Usage</a>
| <a href="/wboc/video.php">Video Views</a>
| <a href="/wboc/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">60</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">60</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">111</td>
	<td align="right">0.30%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">111</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">123</td>
	<td align="right">0.33%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">123</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">90</td>
	<td align="right">0.29%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">92</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">166</td>
	<td align="right">0.50%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">166</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">89</td>
	<td align="right">0.25%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">89</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">360</td>
	<td align="right">0.52%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">362</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">400</td>
	<td align="right">0.70%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">401</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">160</td>
	<td align="right">0.41%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">160</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">280</td>
	<td align="right">0.79%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">283</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">214</td>
	<td align="right">0.59%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">214</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">177</td>
	<td align="right">0.53%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">177</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">203</td>
	<td align="right">0.60%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">203</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">176</td>
	<td align="right">0.57%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">200</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">118</td>
	<td align="right">0.35%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">128</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">41</td>
	<td align="right">0.14%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">41</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">35</td>
	<td align="right">0.13%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">37</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">35</td>
	<td align="right">0.11%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">37</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">23</td>
	<td align="right">0.09%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">66</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
