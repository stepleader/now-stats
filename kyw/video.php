<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KYW Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KYW Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kyw/app/">Premium App</a>
| <a href="/kyw/iapp/">iPhone App</a>
| <a href="/kyw/wap/">Mobile Web</a>
| <a href="/kyw/sms/">SMS Usage</a>
| <a href="/kyw/video.php">Video Views</a>
| <a href="/kyw/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">1,051</td>
	<td align="right">3.62%</td>
	<td align="right">70</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,121</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">1,213</td>
	<td align="right">3.30%</td>
	<td align="right">121</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,334</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">936</td>
	<td align="right">2.49%</td>
	<td align="right">139</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,075</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">957</td>
	<td align="right">3.08%</td>
	<td align="right">170</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,127</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">1,108</td>
	<td align="right">3.34%</td>
	<td align="right">149</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,257</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">1,307</td>
	<td align="right">3.65%</td>
	<td align="right">105</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,412</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,955</td>
	<td align="right">2.81%</td>
	<td align="right">176</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,131</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,693</td>
	<td align="right">2.95%</td>
	<td align="right">293</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,986</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,440</td>
	<td align="right">3.69%</td>
	<td align="right">165</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,605</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">1,350</td>
	<td align="right">3.80%</td>
	<td align="right">123</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,473</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">1,363</td>
	<td align="right">3.75%</td>
	<td align="right">150</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,513</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">1,231</td>
	<td align="right">3.66%</td>
	<td align="right">160</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,391</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">1,328</td>
	<td align="right">3.95%</td>
	<td align="right">118</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,446</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">1,035</td>
	<td align="right">3.35%</td>
	<td align="right">170</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,205</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">1,152</td>
	<td align="right">3.44%</td>
	<td align="right">196</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,348</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">1,174</td>
	<td align="right">4.02%</td>
	<td align="right">199</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,373</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">1,405</td>
	<td align="right">5.33%</td>
	<td align="right">174</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,581</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">1,210</td>
	<td align="right">3.93%</td>
	<td align="right">247</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,457</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">1,078</td>
	<td align="right">4.08%</td>
	<td align="right">83</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,161</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">1,188</td>
	<td align="right">4.02%</td>
	<td align="right">155</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,343</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">2,111</td>
	<td align="right">3.47%</td>
	<td align="right">70</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,181</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">1,346</td>
	<td align="right">1.98%</td>
	<td align="right">99</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,445</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,040</td>
	<td align="right">2.96%</td>
	<td align="right">37</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,077</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,442</td>
	<td align="right">4.45%</td>
	<td align="right">157</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,599</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">1,297</td>
	<td align="right">4.02%</td>
	<td align="right">126</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,423</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,400</td>
	<td align="right">3.93%</td>
	<td align="right">177</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,577</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,135</td>
	<td align="right">3.16%</td>
	<td align="right">277</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,412</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">989</td>
	<td align="right">2.78%</td>
	<td align="right">241</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,230</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">760</td>
	<td align="right">3.09%</td>
	<td align="right">200</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">960</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">745</td>
	<td align="right">3.92%</td>
	<td align="right">144</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">889</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">1,095</td>
	<td align="right">5.39%</td>
	<td align="right">268</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,363</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">1,034</td>
	<td align="right">4.87%</td>
	<td align="right">442</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,476</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">991</td>
	<td align="right">6.09%</td>
	<td align="right">379</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,370</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">544</td>
	<td align="right">5.38%</td>
	<td align="right">251</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">795</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">1</td>
	<td align="right">2.33%</td>
	<td align="right">232</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">233</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">38</td>
	<td align="right">6.95%</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">91</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">23</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
