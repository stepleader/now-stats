<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KATV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KATV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/katv/app/">Premium App</a>
| <a href="/katv/iapp/">iPhone App</a>
| <a href="/katv/wap/">Mobile Web</a>
| <a href="/katv/sms/">SMS Usage</a>
| <a href="/katv/video.php">Video Views</a>
| <a href="/katv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">23</td>
	<td align="right">0.08%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">23</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">107</td>
	<td align="right">0.29%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">110</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">83</td>
	<td align="right">0.22%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">85</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">64</td>
	<td align="right">0.21%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">69</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">80</td>
	<td align="right">0.24%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">81</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">302</td>
	<td align="right">0.84%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">316</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">251</td>
	<td align="right">0.36%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">253</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">336</td>
	<td align="right">0.59%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">354</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">233</td>
	<td align="right">0.60%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">248</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">150</td>
	<td align="right">0.42%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">154</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">209</td>
	<td align="right">0.57%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">214</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">252</td>
	<td align="right">0.75%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">256</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">219</td>
	<td align="right">0.65%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">221</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">158</td>
	<td align="right">0.51%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">161</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">209</td>
	<td align="right">0.62%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">215</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">113</td>
	<td align="right">0.39%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">114</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">109</td>
	<td align="right">0.41%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">112</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">142</td>
	<td align="right">0.46%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">170</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">166</td>
	<td align="right">0.63%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">172</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">136</td>
	<td align="right">0.46%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">151</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">871</td>
	<td align="right">1.43%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">897</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">366</td>
	<td align="right">0.54%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">399</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">328</td>
	<td align="right">0.93%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">346</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">246</td>
	<td align="right">0.76%</td>
	<td align="right">42</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">288</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">264</td>
	<td align="right">0.82%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">310</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">231</td>
	<td align="right">0.65%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">235</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
