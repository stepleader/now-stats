<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WTHR Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WTHR Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wthr/app/">Premium App</a>
| <a href="/wthr/iapp/">iPhone App</a>
| <a href="/wthr/wap/">Mobile Web</a>
| <a href="/wthr/sms/">SMS Usage</a>
| <a href="/wthr/video.php">Video Views</a>
| <a href="/wthr/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">236</td>
	<td align="right">0.81%</td>
	<td align="right">4</td>
	<td align="right">210</td>
	<td align="right">0</td>
	<td align="right">450</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">217</td>
	<td align="right">0.59%</td>
	<td align="right">3</td>
	<td align="right">339</td>
	<td align="right">0</td>
	<td align="right">559</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">308</td>
	<td align="right">0.82%</td>
	<td align="right">6</td>
	<td align="right">299</td>
	<td align="right">0</td>
	<td align="right">613</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">274</td>
	<td align="right">0.88%</td>
	<td align="right">29</td>
	<td align="right">366</td>
	<td align="right">0</td>
	<td align="right">669</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">218</td>
	<td align="right">0.66%</td>
	<td align="right">5</td>
	<td align="right">545</td>
	<td align="right">0</td>
	<td align="right">768</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">256</td>
	<td align="right">0.72%</td>
	<td align="right">12</td>
	<td align="right">573</td>
	<td align="right">0</td>
	<td align="right">841</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">525</td>
	<td align="right">0.75%</td>
	<td align="right">6</td>
	<td align="right">475</td>
	<td align="right">0</td>
	<td align="right">1,006</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">513</td>
	<td align="right">0.89%</td>
	<td align="right">21</td>
	<td align="right">395</td>
	<td align="right">0</td>
	<td align="right">929</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">501</td>
	<td align="right">1.28%</td>
	<td align="right">7</td>
	<td align="right">622</td>
	<td align="right">0</td>
	<td align="right">1,130</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">419</td>
	<td align="right">1.18%</td>
	<td align="right">15</td>
	<td align="right">673</td>
	<td align="right">0</td>
	<td align="right">1,107</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">600</td>
	<td align="right">1.65%</td>
	<td align="right">31</td>
	<td align="right">516</td>
	<td align="right">0</td>
	<td align="right">1,147</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">450</td>
	<td align="right">1.34%</td>
	<td align="right">40</td>
	<td align="right">220</td>
	<td align="right">0</td>
	<td align="right">710</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">324</td>
	<td align="right">0.96%</td>
	<td align="right">26</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">375</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">530</td>
	<td align="right">1.72%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">558</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">635</td>
	<td align="right">1.89%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">641</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">473</td>
	<td align="right">1.62%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">499</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">337</td>
	<td align="right">1.28%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">342</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">516</td>
	<td align="right">1.68%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">538</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">431</td>
	<td align="right">1.63%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">450</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">391</td>
	<td align="right">1.32%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">440</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,134</td>
	<td align="right">1.86%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,180</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">707</td>
	<td align="right">1.04%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">731</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">598</td>
	<td align="right">1.70%</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">629</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">725</td>
	<td align="right">2.24%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">776</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">625</td>
	<td align="right">1.94%</td>
	<td align="right">101</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">726</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">813</td>
	<td align="right">2.28%</td>
	<td align="right">80</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">893</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">961</td>
	<td align="right">2.67%</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">992</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">989</td>
	<td align="right">2.78%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,053</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">484</td>
	<td align="right">1.97%</td>
	<td align="right">82</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">566</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">317</td>
	<td align="right">1.67%</td>
	<td align="right">138</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">455</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">245</td>
	<td align="right">1.21%</td>
	<td align="right">76</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">321</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">484</td>
	<td align="right">2.28%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">524</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">203</td>
	<td align="right">1.25%</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">242</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
