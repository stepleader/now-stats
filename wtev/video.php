<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WTEV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WTEV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wtev/app/">Premium App</a>
| <a href="/wtev/iapp/">iPhone App</a>
| <a href="/wtev/wap/">Mobile Web</a>
| <a href="/wtev/sms/">SMS Usage</a>
| <a href="/wtev/video.php">Video Views</a>
| <a href="/wtev/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">338</td>
	<td align="right">1.16%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">338</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">338</td>
	<td align="right">0.92%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">352</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">463</td>
	<td align="right">1.23%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">478</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">366</td>
	<td align="right">1.18%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">372</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">271</td>
	<td align="right">0.82%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">274</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">403</td>
	<td align="right">1.13%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">436</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">684</td>
	<td align="right">0.98%</td>
	<td align="right">74</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">758</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,084</td>
	<td align="right">1.89%</td>
	<td align="right">134</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,218</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">565</td>
	<td align="right">1.45%</td>
	<td align="right">65</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">630</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">400</td>
	<td align="right">1.13%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">401</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">426</td>
	<td align="right">1.17%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">432</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">305</td>
	<td align="right">0.91%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">324</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">308</td>
	<td align="right">0.92%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">335</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">334</td>
	<td align="right">1.08%</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">365</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">310</td>
	<td align="right">0.92%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">321</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">346</td>
	<td align="right">1.18%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">380</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">123</td>
	<td align="right">0.47%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">144</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">11</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
