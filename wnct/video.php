<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WNCT Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WNCT Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wnct/app/">Premium App</a>
| <a href="/wnct/iapp/">iPhone App</a>
| <a href="/wnct/wap/">Mobile Web</a>
| <a href="/wnct/sms/">SMS Usage</a>
| <a href="/wnct/video.php">Video Views</a>
| <a href="/wnct/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">78</td>
	<td align="right">0.27%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">78</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">77</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">77</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">57</td>
	<td align="right">0.15%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">57</td>
	<td align="right">0.18%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">66</td>
	<td align="right">0.20%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">66</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">121</td>
	<td align="right">0.34%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">126</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">422</td>
	<td align="right">0.61%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">424</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">247</td>
	<td align="right">0.43%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">259</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">95</td>
	<td align="right">0.24%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">96</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">75</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">75</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">127</td>
	<td align="right">0.35%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">131</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">135</td>
	<td align="right">0.40%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">138</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">114</td>
	<td align="right">0.34%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">114</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">65</td>
	<td align="right">0.21%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">67</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">87</td>
	<td align="right">0.26%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">91</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">42</td>
	<td align="right">0.14%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">52</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">33</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">188</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">188</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
