<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KSDK Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KSDK Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/ksdk/app/">Premium App</a>
| <a href="/ksdk/iapp/">iPhone App</a>
| <a href="/ksdk/wap/">Mobile Web</a>
| <a href="/ksdk/sms/">SMS Usage</a>
| <a href="/ksdk/video.php">Video Views</a>
| <a href="/ksdk/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">382</td>
	<td align="right">1.32%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">393</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">677</td>
	<td align="right">1.84%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">695</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">587</td>
	<td align="right">1.56%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">609</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">573</td>
	<td align="right">1.84%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">616</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">581</td>
	<td align="right">1.75%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">603</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">479</td>
	<td align="right">1.34%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">506</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">852</td>
	<td align="right">1.23%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">875</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">634</td>
	<td align="right">1.11%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">680</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">428</td>
	<td align="right">1.10%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">439</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">611</td>
	<td align="right">1.72%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">629</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">542</td>
	<td align="right">1.49%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">556</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">610</td>
	<td align="right">1.81%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">626</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">775</td>
	<td align="right">2.31%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">794</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">725</td>
	<td align="right">2.35%</td>
	<td align="right">75</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">800</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">414</td>
	<td align="right">1.23%</td>
	<td align="right">160</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">574</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">425</td>
	<td align="right">1.46%</td>
	<td align="right">96</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">521</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">373</td>
	<td align="right">1.42%</td>
	<td align="right">56</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">429</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">451</td>
	<td align="right">1.46%</td>
	<td align="right">56</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">507</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">480</td>
	<td align="right">1.82%</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">532</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">541</td>
	<td align="right">1.83%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">610</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,013</td>
	<td align="right">1.66%</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,091</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">681</td>
	<td align="right">1.00%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">699</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">545</td>
	<td align="right">1.55%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">581</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">662</td>
	<td align="right">2.04%</td>
	<td align="right">77</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">739</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">689</td>
	<td align="right">2.14%</td>
	<td align="right">97</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">786</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">932</td>
	<td align="right">2.62%</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,010</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">877</td>
	<td align="right">2.44%</td>
	<td align="right">108</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">985</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">871</td>
	<td align="right">2.45%</td>
	<td align="right">73</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">944</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">60</td>
	<td align="right">0.24%</td>
	<td align="right">143</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">203</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">74</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">74</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">20</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
