<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WSMV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WSMV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wsmv/app/">Premium App</a>
| <a href="/wsmv/iapp/">iPhone App</a>
| <a href="/wsmv/wap/">Mobile Web</a>
| <a href="/wsmv/sms/">SMS Usage</a>
| <a href="/wsmv/video.php">Video Views</a>
| <a href="/wsmv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">223</td>
	<td align="right">0.77%</td>
	<td align="right">1</td>
	<td align="right">103</td>
	<td align="right">0</td>
	<td align="right">327</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">293</td>
	<td align="right">0.80%</td>
	<td align="right">7</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">352</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">234</td>
	<td align="right">0.62%</td>
	<td align="right">17</td>
	<td align="right">79</td>
	<td align="right">0</td>
	<td align="right">330</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">281</td>
	<td align="right">0.90%</td>
	<td align="right">5</td>
	<td align="right">62</td>
	<td align="right">0</td>
	<td align="right">348</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">201</td>
	<td align="right">0.61%</td>
	<td align="right">7</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">254</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">353</td>
	<td align="right">0.99%</td>
	<td align="right">7</td>
	<td align="right">66</td>
	<td align="right">0</td>
	<td align="right">426</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">592</td>
	<td align="right">0.85%</td>
	<td align="right">22</td>
	<td align="right">55</td>
	<td align="right">0</td>
	<td align="right">669</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">552</td>
	<td align="right">0.96%</td>
	<td align="right">33</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">629</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">418</td>
	<td align="right">1.07%</td>
	<td align="right">9</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">458</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">431</td>
	<td align="right">1.21%</td>
	<td align="right">1</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">471</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">392</td>
	<td align="right">1.08%</td>
	<td align="right">1</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">427</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">292</td>
	<td align="right">0.87%</td>
	<td align="right">13</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">328</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">442</td>
	<td align="right">1.32%</td>
	<td align="right">18</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">492</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">348</td>
	<td align="right">1.13%</td>
	<td align="right">33</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">406</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">356</td>
	<td align="right">1.06%</td>
	<td align="right">13</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">376</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">265</td>
	<td align="right">0.91%</td>
	<td align="right">88</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">353</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">340</td>
	<td align="right">1.29%</td>
	<td align="right">41</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">381</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">257</td>
	<td align="right">0.83%</td>
	<td align="right">20</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">280</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">243</td>
	<td align="right">0.92%</td>
	<td align="right">13</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">261</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">251</td>
	<td align="right">0.85%</td>
	<td align="right">36</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">291</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">875</td>
	<td align="right">1.44%</td>
	<td align="right">73</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">949</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">465</td>
	<td align="right">0.68%</td>
	<td align="right">81</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">546</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">481</td>
	<td align="right">1.37%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">515</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">598</td>
	<td align="right">1.85%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">615</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">651</td>
	<td align="right">2.02%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">683</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">765</td>
	<td align="right">2.15%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">810</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">678</td>
	<td align="right">1.89%</td>
	<td align="right">60</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">738</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">803</td>
	<td align="right">2.26%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">829</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">467</td>
	<td align="right">1.90%</td>
	<td align="right">58</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">525</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">259</td>
	<td align="right">1.36%</td>
	<td align="right">121</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">380</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
