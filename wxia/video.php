<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WXIA Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WXIA Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wxia/app/">Premium App</a>
| <a href="/wxia/iapp/">iPhone App</a>
| <a href="/wxia/wap/">Mobile Web</a>
| <a href="/wxia/sms/">SMS Usage</a>
| <a href="/wxia/video.php">Video Views</a>
| <a href="/wxia/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">365</td>
	<td align="right">1.26%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">392</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">391</td>
	<td align="right">1.07%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">419</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">452</td>
	<td align="right">1.20%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">467</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">475</td>
	<td align="right">1.53%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">496</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">546</td>
	<td align="right">1.65%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">595</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">610</td>
	<td align="right">1.71%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">645</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,084</td>
	<td align="right">1.56%</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,145</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,127</td>
	<td align="right">1.96%</td>
	<td align="right">68</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,195</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,239</td>
	<td align="right">3.17%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,263</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">1,132</td>
	<td align="right">3.19%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,154</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">1,149</td>
	<td align="right">3.16%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,174</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">1,491</td>
	<td align="right">4.43%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,517</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">1,912</td>
	<td align="right">5.69%</td>
	<td align="right">122</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,035</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">1,378</td>
	<td align="right">4.46%</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,430</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">1,651</td>
	<td align="right">4.92%</td>
	<td align="right">82</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,733</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">1,293</td>
	<td align="right">4.43%</td>
	<td align="right">84</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,377</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">1,170</td>
	<td align="right">4.44%</td>
	<td align="right">77</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,250</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">1,886</td>
	<td align="right">6.12%</td>
	<td align="right">176</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,062</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">1,952</td>
	<td align="right">7.39%</td>
	<td align="right">120</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,072</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">1,663</td>
	<td align="right">5.63%</td>
	<td align="right">502</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,177</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">3,002</td>
	<td align="right">4.93%</td>
	<td align="right">524</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,526</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">1,993</td>
	<td align="right">2.94%</td>
	<td align="right">377</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,370</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,670</td>
	<td align="right">4.76%</td>
	<td align="right">322</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,992</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">496</td>
	<td align="right">1.53%</td>
	<td align="right">447</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">943</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
