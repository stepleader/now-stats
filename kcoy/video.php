<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KCOY Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KCOY Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kcoy/app/">Premium App</a>
| <a href="/kcoy/iapp/">iPhone App</a>
| <a href="/kcoy/wap/">Mobile Web</a>
| <a href="/kcoy/sms/">SMS Usage</a>
| <a href="/kcoy/video.php">Video Views</a>
| <a href="/kcoy/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">90</td>
	<td align="right">0.31%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">90</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">109</td>
	<td align="right">0.30%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">109</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">79</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">79</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">70</td>
	<td align="right">0.23%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">75</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">114</td>
	<td align="right">0.34%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">117</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">224</td>
	<td align="right">0.63%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">227</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">373</td>
	<td align="right">0.54%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">377</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">480</td>
	<td align="right">0.84%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">481</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">252</td>
	<td align="right">0.65%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">252</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">129</td>
	<td align="right">0.36%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">129</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">112</td>
	<td align="right">0.31%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">112</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">97</td>
	<td align="right">0.29%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">97</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">112</td>
	<td align="right">0.33%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">112</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">64</td>
	<td align="right">0.21%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">66</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">17</td>
	<td align="right">0.05%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">35</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">5</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
