<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KOMO Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KOMO Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/komo/app/">Premium App</a>
| <a href="/komo/iapp/">iPhone App</a>
| <a href="/komo/wap/">Mobile Web</a>
| <a href="/komo/sms/">SMS Usage</a>
| <a href="/komo/video.php">Video Views</a>
| <a href="/komo/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">309</td>
	<td align="right">1.06%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">313</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">420</td>
	<td align="right">1.14%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">434</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">507</td>
	<td align="right">1.35%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">517</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">368</td>
	<td align="right">1.18%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">380</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">408</td>
	<td align="right">1.23%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">413</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">499</td>
	<td align="right">1.40%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">528</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">679</td>
	<td align="right">0.98%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">723</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">526</td>
	<td align="right">0.92%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">569</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">511</td>
	<td align="right">1.31%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">525</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">515</td>
	<td align="right">1.45%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">538</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">440</td>
	<td align="right">1.21%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">452</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">260</td>
	<td align="right">0.77%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">263</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">293</td>
	<td align="right">0.87%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">301</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">303</td>
	<td align="right">0.98%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">324</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">322</td>
	<td align="right">0.96%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">347</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">334</td>
	<td align="right">1.14%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">353</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">335</td>
	<td align="right">1.27%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">365</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">396</td>
	<td align="right">1.29%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">416</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">262</td>
	<td align="right">0.99%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">307</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">320</td>
	<td align="right">1.08%</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">373</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">852</td>
	<td align="right">1.40%</td>
	<td align="right">42</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">894</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">196</td>
	<td align="right">0.29%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">239</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">4</td>
	<td align="right">0.01%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">49</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
