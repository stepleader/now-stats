<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KYTX Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KYTX Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kytx/app/">Premium App</a>
| <a href="/kytx/iapp/">iPhone App</a>
| <a href="/kytx/wap/">Mobile Web</a>
| <a href="/kytx/sms/">SMS Usage</a>
| <a href="/kytx/video.php">Video Views</a>
| <a href="/kytx/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">48</td>
	<td align="right">0.17%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">48</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">62</td>
	<td align="right">0.17%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">63</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">71</td>
	<td align="right">0.19%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">71</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">86</td>
	<td align="right">0.28%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">88</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">98</td>
	<td align="right">0.30%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">98</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">100</td>
	<td align="right">0.28%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">100</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">621</td>
	<td align="right">0.89%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">638</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">235</td>
	<td align="right">0.41%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">239</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">187</td>
	<td align="right">0.48%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">219</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">154</td>
	<td align="right">0.43%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">205</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">215</td>
	<td align="right">0.59%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">260</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">159</td>
	<td align="right">0.47%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">208</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">165</td>
	<td align="right">0.49%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">175</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">103</td>
	<td align="right">0.33%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">105</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">72</td>
	<td align="right">0.21%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">90</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">105</td>
	<td align="right">0.36%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">117</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">15</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">35</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
