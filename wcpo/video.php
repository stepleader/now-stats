<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WCPO Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WCPO Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wcpo/app/">Premium App</a>
| <a href="/wcpo/iapp/">iPhone App</a>
| <a href="/wcpo/wap/">Mobile Web</a>
| <a href="/wcpo/sms/">SMS Usage</a>
| <a href="/wcpo/video.php">Video Views</a>
| <a href="/wcpo/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">229</td>
	<td align="right">0.79%</td>
	<td align="right">1</td>
	<td align="right">203</td>
	<td align="right">0</td>
	<td align="right">433</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">298</td>
	<td align="right">0.81%</td>
	<td align="right">2</td>
	<td align="right">400</td>
	<td align="right">0</td>
	<td align="right">700</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">351</td>
	<td align="right">0.93%</td>
	<td align="right">14</td>
	<td align="right">728</td>
	<td align="right">0</td>
	<td align="right">1,093</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">209</td>
	<td align="right">0.67%</td>
	<td align="right">5</td>
	<td align="right">229</td>
	<td align="right">0</td>
	<td align="right">443</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">193</td>
	<td align="right">0.58%</td>
	<td align="right">1</td>
	<td align="right">62</td>
	<td align="right">0</td>
	<td align="right">256</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">584</td>
	<td align="right">1.63%</td>
	<td align="right">4</td>
	<td align="right">100</td>
	<td align="right">0</td>
	<td align="right">688</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,319</td>
	<td align="right">1.90%</td>
	<td align="right">11</td>
	<td align="right">115</td>
	<td align="right">0</td>
	<td align="right">1,445</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">173</td>
	<td align="right">0.30%</td>
	<td align="right">35</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">221</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
