<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KUTV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KUTV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kutv/app/">Premium App</a>
| <a href="/kutv/iapp/">iPhone App</a>
| <a href="/kutv/wap/">Mobile Web</a>
| <a href="/kutv/sms/">SMS Usage</a>
| <a href="/kutv/video.php">Video Views</a>
| <a href="/kutv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">206</td>
	<td align="right">0.71%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">208</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">246</td>
	<td align="right">0.67%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">249</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">321</td>
	<td align="right">0.85%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">321</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">260</td>
	<td align="right">0.84%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">261</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">291</td>
	<td align="right">0.88%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">293</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">349</td>
	<td align="right">0.98%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">382</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">466</td>
	<td align="right">0.67%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">491</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">538</td>
	<td align="right">0.94%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">555</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">566</td>
	<td align="right">1.45%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">567</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">720</td>
	<td align="right">2.03%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">736</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">482</td>
	<td align="right">1.33%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">483</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">569</td>
	<td align="right">1.69%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">573</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">527</td>
	<td align="right">1.57%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">539</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">561</td>
	<td align="right">1.82%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">612</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">665</td>
	<td align="right">1.98%</td>
	<td align="right">95</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">760</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">620</td>
	<td align="right">2.12%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">639</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">549</td>
	<td align="right">2.08%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">552</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">471</td>
	<td align="right">1.53%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">475</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">637</td>
	<td align="right">2.41%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">657</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">794</td>
	<td align="right">2.69%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">824</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">2,106</td>
	<td align="right">3.46%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,123</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">909</td>
	<td align="right">1.34%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">921</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">560</td>
	<td align="right">1.60%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">572</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">444</td>
	<td align="right">1.37%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">469</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">782</td>
	<td align="right">2.43%</td>
	<td align="right">47</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">829</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">839</td>
	<td align="right">2.36%</td>
	<td align="right">77</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">916</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">812</td>
	<td align="right">2.26%</td>
	<td align="right">67</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">879</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">786</td>
	<td align="right">2.21%</td>
	<td align="right">229</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,015</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">600</td>
	<td align="right">2.44%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">636</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">364</td>
	<td align="right">1.91%</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">416</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">484</td>
	<td align="right">2.38%</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">523</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">670</td>
	<td align="right">3.15%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">713</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">648</td>
	<td align="right">3.98%</td>
	<td align="right">126</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">774</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">216</td>
	<td align="right">2.14%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">237</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">1</td>
	<td align="right">2.33%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">8</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">30</td>
	<td align="right">5.48%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">49</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">22</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
