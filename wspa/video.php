<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WSPA Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WSPA Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wspa/app/">Premium App</a>
| <a href="/wspa/iapp/">iPhone App</a>
| <a href="/wspa/wap/">Mobile Web</a>
| <a href="/wspa/sms/">SMS Usage</a>
| <a href="/wspa/video.php">Video Views</a>
| <a href="/wspa/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">57</td>
	<td align="right">0.15%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">86</td>
	<td align="right">0.24%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">86</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">133</td>
	<td align="right">0.37%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">133</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">229</td>
	<td align="right">0.68%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">233</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">71</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">71</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">149</td>
	<td align="right">0.48%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">151</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">208</td>
	<td align="right">0.62%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">216</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">172</td>
	<td align="right">0.59%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">177</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">122</td>
	<td align="right">0.46%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">128</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">217</td>
	<td align="right">0.70%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">253</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">143</td>
	<td align="right">0.54%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">167</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">118</td>
	<td align="right">0.40%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">128</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">871</td>
	<td align="right">1.43%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">892</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">363</td>
	<td align="right">0.53%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">365</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">349</td>
	<td align="right">0.99%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">360</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">119</td>
	<td align="right">0.37%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">142</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">264</td>
	<td align="right">0.82%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">277</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">461</td>
	<td align="right">1.29%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">476</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">544</td>
	<td align="right">1.51%</td>
	<td align="right">85</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">629</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">25</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
