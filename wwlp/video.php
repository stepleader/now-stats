<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WWLP Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WWLP Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wwlp/app/">Premium App</a>
| <a href="/wwlp/iapp/">iPhone App</a>
| <a href="/wwlp/wap/">Mobile Web</a>
| <a href="/wwlp/sms/">SMS Usage</a>
| <a href="/wwlp/video.php">Video Views</a>
| <a href="/wwlp/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">70</td>
	<td align="right">0.24%</td>
	<td align="right">0</td>
	<td align="right">113</td>
	<td align="right">0</td>
	<td align="right">183</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">82</td>
	<td align="right">0.22%</td>
	<td align="right">0</td>
	<td align="right">113</td>
	<td align="right">0</td>
	<td align="right">195</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">80</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">123</td>
	<td align="right">0</td>
	<td align="right">203</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">122</td>
	<td align="right">0.39%</td>
	<td align="right">0</td>
	<td align="right">91</td>
	<td align="right">0</td>
	<td align="right">213</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">79</td>
	<td align="right">0.24%</td>
	<td align="right">2</td>
	<td align="right">80</td>
	<td align="right">0</td>
	<td align="right">161</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">57</td>
	<td align="right">0.16%</td>
	<td align="right">0</td>
	<td align="right">210</td>
	<td align="right">0</td>
	<td align="right">267</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">351</td>
	<td align="right">0.50%</td>
	<td align="right">1</td>
	<td align="right">139</td>
	<td align="right">0</td>
	<td align="right">491</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">270</td>
	<td align="right">0.47%</td>
	<td align="right">9</td>
	<td align="right">114</td>
	<td align="right">0</td>
	<td align="right">393</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">103</td>
	<td align="right">0.26%</td>
	<td align="right">0</td>
	<td align="right">234</td>
	<td align="right">0</td>
	<td align="right">337</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">219</td>
	<td align="right">0.62%</td>
	<td align="right">0</td>
	<td align="right">163</td>
	<td align="right">0</td>
	<td align="right">382</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">106</td>
	<td align="right">0.29%</td>
	<td align="right">4</td>
	<td align="right">56</td>
	<td align="right">0</td>
	<td align="right">166</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">170</td>
	<td align="right">0.51%</td>
	<td align="right">0</td>
	<td align="right">59</td>
	<td align="right">0</td>
	<td align="right">229</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">179</td>
	<td align="right">0.53%</td>
	<td align="right">0</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">257</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">89</td>
	<td align="right">0.29%</td>
	<td align="right">0</td>
	<td align="right">202</td>
	<td align="right">0</td>
	<td align="right">291</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">137</td>
	<td align="right">0.41%</td>
	<td align="right">0</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">140</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">158</td>
	<td align="right">0.54%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">158</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">51</td>
	<td align="right">0.19%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">97</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">12</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
