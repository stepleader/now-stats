<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WFMY Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WFMY Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wfmy/app/">Premium App</a>
| <a href="/wfmy/iapp/">iPhone App</a>
| <a href="/wfmy/wap/">Mobile Web</a>
| <a href="/wfmy/sms/">SMS Usage</a>
| <a href="/wfmy/video.php">Video Views</a>
| <a href="/wfmy/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">54</td>
	<td align="right">0.19%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">54</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">130</td>
	<td align="right">0.35%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">131</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">150</td>
	<td align="right">0.40%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">177</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">99</td>
	<td align="right">0.32%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">111</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">79</td>
	<td align="right">0.24%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">81</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">62</td>
	<td align="right">0.17%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">65</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">250</td>
	<td align="right">0.36%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">255</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">260</td>
	<td align="right">0.45%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">270</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">227</td>
	<td align="right">0.58%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">236</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">136</td>
	<td align="right">0.38%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">149</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">250</td>
	<td align="right">0.69%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">274</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">123</td>
	<td align="right">0.37%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">128</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">162</td>
	<td align="right">0.48%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">174</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">247</td>
	<td align="right">0.80%</td>
	<td align="right">42</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">289</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">315</td>
	<td align="right">0.94%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">339</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">224</td>
	<td align="right">0.77%</td>
	<td align="right">59</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">283</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">111</td>
	<td align="right">0.42%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">175</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">147</td>
	<td align="right">0.48%</td>
	<td align="right">62</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">209</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">100</td>
	<td align="right">0.38%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">115</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">84</td>
	<td align="right">0.28%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">102</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">610</td>
	<td align="right">1.00%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">626</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">2,307</td>
	<td align="right">3.40%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,319</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">206</td>
	<td align="right">0.59%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">210</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">271</td>
	<td align="right">0.84%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">304</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">393</td>
	<td align="right">1.22%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">427</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">516</td>
	<td align="right">1.45%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">525</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">662</td>
	<td align="right">1.84%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">684</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">585</td>
	<td align="right">1.65%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">597</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">262</td>
	<td align="right">1.06%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">279</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">90</td>
	<td align="right">0.47%</td>
	<td align="right">210</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">300</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">119</td>
	<td align="right">0.59%</td>
	<td align="right">287</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">406</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">185</td>
	<td align="right">0.87%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">233</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">223</td>
	<td align="right">1.37%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">246</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">163</td>
	<td align="right">1.61%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">220</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">3</td>
	<td align="right">6.98%</td>
	<td align="right">75</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">78</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">55</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">55</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
