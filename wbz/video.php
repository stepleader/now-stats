<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WBZ Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WBZ Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wbz/app/">Premium App</a>
| <a href="/wbz/iapp/">iPhone App</a>
| <a href="/wbz/wap/">Mobile Web</a>
| <a href="/wbz/sms/">SMS Usage</a>
| <a href="/wbz/video.php">Video Views</a>
| <a href="/wbz/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">318</td>
	<td align="right">1.10%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">326</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">310</td>
	<td align="right">0.84%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">317</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">470</td>
	<td align="right">1.25%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">470</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">621</td>
	<td align="right">2.00%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">622</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">313</td>
	<td align="right">0.94%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">327</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">497</td>
	<td align="right">1.39%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">521</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,056</td>
	<td align="right">1.52%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,088</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,043</td>
	<td align="right">1.82%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,094</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">942</td>
	<td align="right">2.41%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">951</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">954</td>
	<td align="right">2.69%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">963</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">966</td>
	<td align="right">2.66%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">970</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">755</td>
	<td align="right">2.25%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">776</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">871</td>
	<td align="right">2.59%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">893</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">894</td>
	<td align="right">2.90%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">924</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">1,117</td>
	<td align="right">3.33%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,132</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">1,208</td>
	<td align="right">4.14%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,230</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">829</td>
	<td align="right">3.15%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">840</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">951</td>
	<td align="right">3.09%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">969</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">789</td>
	<td align="right">2.99%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">827</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">807</td>
	<td align="right">2.73%</td>
	<td align="right">81</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">888</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">3,006</td>
	<td align="right">4.94%</td>
	<td align="right">87</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,093</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">2,604</td>
	<td align="right">3.84%</td>
	<td align="right">66</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,670</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,124</td>
	<td align="right">3.20%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,181</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,056</td>
	<td align="right">3.26%</td>
	<td align="right">96</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,152</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">1,321</td>
	<td align="right">4.10%</td>
	<td align="right">124</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,445</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,333</td>
	<td align="right">3.74%</td>
	<td align="right">90</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,423</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,189</td>
	<td align="right">3.31%</td>
	<td align="right">98</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,287</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,267</td>
	<td align="right">3.56%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,330</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">887</td>
	<td align="right">3.60%</td>
	<td align="right">106</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">993</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">683</td>
	<td align="right">3.59%</td>
	<td align="right">95</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">778</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">551</td>
	<td align="right">2.71%</td>
	<td align="right">60</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">611</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">808</td>
	<td align="right">3.80%</td>
	<td align="right">132</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">940</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">551</td>
	<td align="right">3.39%</td>
	<td align="right">225</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">776</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">433</td>
	<td align="right">4.29%</td>
	<td align="right">62</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">495</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">51</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">28</td>
	<td align="right">5.12%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">14</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
