<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KDKA Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KDKA Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kdka/app/">Premium App</a>
| <a href="/kdka/iapp/">iPhone App</a>
| <a href="/kdka/wap/">Mobile Web</a>
| <a href="/kdka/sms/">SMS Usage</a>
| <a href="/kdka/video.php">Video Views</a>
| <a href="/kdka/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">264</td>
	<td align="right">0.91%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">269</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">417</td>
	<td align="right">1.14%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">423</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">322</td>
	<td align="right">0.86%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">335</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">316</td>
	<td align="right">1.02%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">360</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">520</td>
	<td align="right">1.57%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">549</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">548</td>
	<td align="right">1.53%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">634</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">689</td>
	<td align="right">0.99%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">746</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">550</td>
	<td align="right">0.96%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">574</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">417</td>
	<td align="right">1.07%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">426</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">383</td>
	<td align="right">1.08%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">419</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">345</td>
	<td align="right">0.95%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">378</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">503</td>
	<td align="right">1.50%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">507</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">387</td>
	<td align="right">1.15%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">389</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">404</td>
	<td align="right">1.31%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">406</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">408</td>
	<td align="right">1.22%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">417</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">293</td>
	<td align="right">1.00%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">297</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">264</td>
	<td align="right">1.00%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">267</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">213</td>
	<td align="right">0.69%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">226</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">369</td>
	<td align="right">1.40%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">398</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">398</td>
	<td align="right">1.35%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">455</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,039</td>
	<td align="right">1.71%</td>
	<td align="right">90</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,129</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">607</td>
	<td align="right">0.89%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">645</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">606</td>
	<td align="right">1.73%</td>
	<td align="right">58</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">664</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">543</td>
	<td align="right">1.68%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">600</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">445</td>
	<td align="right">1.38%</td>
	<td align="right">190</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">635</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">809</td>
	<td align="right">2.27%</td>
	<td align="right">158</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">967</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">809</td>
	<td align="right">2.25%</td>
	<td align="right">132</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">941</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">853</td>
	<td align="right">2.40%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">898</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">536</td>
	<td align="right">2.18%</td>
	<td align="right">88</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">624</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">389</td>
	<td align="right">2.05%</td>
	<td align="right">37</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">426</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">400</td>
	<td align="right">1.97%</td>
	<td align="right">198</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">598</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">532</td>
	<td align="right">2.50%</td>
	<td align="right">116</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">648</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">623</td>
	<td align="right">3.83%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">655</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">358</td>
	<td align="right">3.54%</td>
	<td align="right">54</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">412</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">1</td>
	<td align="right">2.33%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">12</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">16</td>
	<td align="right">2.93%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">30</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
