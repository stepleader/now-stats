<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KPIX Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KPIX Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kpix/app/">Premium App</a>
| <a href="/kpix/iapp/">iPhone App</a>
| <a href="/kpix/wap/">Mobile Web</a>
| <a href="/kpix/sms/">SMS Usage</a>
| <a href="/kpix/video.php">Video Views</a>
| <a href="/kpix/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">423</td>
	<td align="right">1.46%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">429</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">661</td>
	<td align="right">1.80%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">669</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">927</td>
	<td align="right">2.47%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">941</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">832</td>
	<td align="right">2.68%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">867</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">1,345</td>
	<td align="right">4.06%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,361</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">1,207</td>
	<td align="right">3.37%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,221</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,401</td>
	<td align="right">2.01%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,413</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,157</td>
	<td align="right">2.02%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,184</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,127</td>
	<td align="right">2.89%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,151</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">1,053</td>
	<td align="right">2.96%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,078</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">1,058</td>
	<td align="right">2.91%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,066</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">833</td>
	<td align="right">2.48%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">851</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">729</td>
	<td align="right">2.17%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">774</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">833</td>
	<td align="right">2.70%</td>
	<td align="right">50</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">883</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">1,375</td>
	<td align="right">4.10%</td>
	<td align="right">59</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,434</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">946</td>
	<td align="right">3.24%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">980</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">702</td>
	<td align="right">2.67%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">729</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">929</td>
	<td align="right">3.02%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">973</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">878</td>
	<td align="right">3.33%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">895</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">900</td>
	<td align="right">3.04%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">950</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,951</td>
	<td align="right">3.20%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,010</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">1,015</td>
	<td align="right">1.50%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,078</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,002</td>
	<td align="right">2.85%</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,063</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,108</td>
	<td align="right">3.42%</td>
	<td align="right">71</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,179</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">945</td>
	<td align="right">2.93%</td>
	<td align="right">225</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,170</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,311</td>
	<td align="right">3.68%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,397</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,534</td>
	<td align="right">4.27%</td>
	<td align="right">170</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,704</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,755</td>
	<td align="right">4.94%</td>
	<td align="right">177</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,932</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">1,032</td>
	<td align="right">4.19%</td>
	<td align="right">151</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,183</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">926</td>
	<td align="right">4.87%</td>
	<td align="right">273</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,199</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">763</td>
	<td align="right">3.76%</td>
	<td align="right">89</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">852</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">926</td>
	<td align="right">4.36%</td>
	<td align="right">104</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,030</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">722</td>
	<td align="right">4.44%</td>
	<td align="right">132</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">854</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">407</td>
	<td align="right">4.03%</td>
	<td align="right">240</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">647</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">109</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">109</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">20</td>
	<td align="right">3.66%</td>
	<td align="right">77</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">97</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">24</td>
</tr>
<tr>
	<td>Feb. 2006</td>
	<td align="right">0</td>
	<td align="right">0%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">23</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
