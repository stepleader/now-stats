<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WLFI Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WLFI Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wlfi/app/">Premium App</a>
| <a href="/wlfi/iapp/">iPhone App</a>
| <a href="/wlfi/wap/">Mobile Web</a>
| <a href="/wlfi/sms/">SMS Usage</a>
| <a href="/wlfi/video.php">Video Views</a>
| <a href="/wlfi/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">151</td>
	<td align="right">0.52%</td>
	<td align="right">0</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">171</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">136</td>
	<td align="right">0.37%</td>
	<td align="right">0</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">165</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">70</td>
	<td align="right">0.19%</td>
	<td align="right">0</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">93</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">58</td>
	<td align="right">0.19%</td>
	<td align="right">0</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">107</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">26</td>
	<td align="right">0.08%</td>
	<td align="right">0</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">43</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">18</td>
	<td align="right">0.05%</td>
	<td align="right">3</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">66</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">152</td>
	<td align="right">0.22%</td>
	<td align="right">1</td>
	<td align="right">37</td>
	<td align="right">0</td>
	<td align="right">190</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">228</td>
	<td align="right">0.40%</td>
	<td align="right">0</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">240</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">88</td>
	<td align="right">0.23%</td>
	<td align="right">0</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">99</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">100</td>
	<td align="right">0.28%</td>
	<td align="right">0</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">138</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">128</td>
	<td align="right">0.35%</td>
	<td align="right">1</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">140</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">169</td>
	<td align="right">0.50%</td>
	<td align="right">0</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">183</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">127</td>
	<td align="right">0.38%</td>
	<td align="right">0</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">150</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">171</td>
	<td align="right">0.55%</td>
	<td align="right">0</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">207</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">187</td>
	<td align="right">0.56%</td>
	<td align="right">0</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">190</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">179</td>
	<td align="right">0.61%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">179</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">62</td>
	<td align="right">0.24%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">85</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">10</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
