<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KCBS Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KCBS Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kcbs/app/">Premium App</a>
| <a href="/kcbs/iapp/">iPhone App</a>
| <a href="/kcbs/wap/">Mobile Web</a>
| <a href="/kcbs/sms/">SMS Usage</a>
| <a href="/kcbs/video.php">Video Views</a>
| <a href="/kcbs/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">1,647</td>
	<td align="right">5.68%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,669</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">2,510</td>
	<td align="right">6.84%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,573</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">2,257</td>
	<td align="right">6.00%</td>
	<td align="right">103</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,360</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">1,811</td>
	<td align="right">5.83%</td>
	<td align="right">261</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,072</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">2,865</td>
	<td align="right">8.64%</td>
	<td align="right">243</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,108</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">2,494</td>
	<td align="right">6.97%</td>
	<td align="right">307</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,801</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">3,210</td>
	<td align="right">4.62%</td>
	<td align="right">501</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,711</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">3,378</td>
	<td align="right">5.89%</td>
	<td align="right">520</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,898</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">2,926</td>
	<td align="right">7.49%</td>
	<td align="right">185</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,111</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">2,815</td>
	<td align="right">7.92%</td>
	<td align="right">212</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,027</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">2,542</td>
	<td align="right">6.99%</td>
	<td align="right">342</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">2,885</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">1,947</td>
	<td align="right">5.79%</td>
	<td align="right">408</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,355</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">1,947</td>
	<td align="right">5.79%</td>
	<td align="right">366</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,313</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">2,052</td>
	<td align="right">6.65%</td>
	<td align="right">256</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,308</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">2,389</td>
	<td align="right">7.13%</td>
	<td align="right">303</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,692</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">1,555</td>
	<td align="right">5.32%</td>
	<td align="right">267</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,822</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">1,783</td>
	<td align="right">6.77%</td>
	<td align="right">157</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,940</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">3,094</td>
	<td align="right">10.05%</td>
	<td align="right">385</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,479</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">2,031</td>
	<td align="right">7.69%</td>
	<td align="right">448</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,479</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">2,212</td>
	<td align="right">7.48%</td>
	<td align="right">513</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,725</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">3,120</td>
	<td align="right">5.12%</td>
	<td align="right">523</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,643</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">2,990</td>
	<td align="right">4.40%</td>
	<td align="right">702</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,692</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">3,315</td>
	<td align="right">9.44%</td>
	<td align="right">232</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,547</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">3,163</td>
	<td align="right">9.76%</td>
	<td align="right">299</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,462</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">3,330</td>
	<td align="right">10.33%</td>
	<td align="right">357</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,687</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">3,453</td>
	<td align="right">9.70%</td>
	<td align="right">354</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,807</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">3,564</td>
	<td align="right">9.92%</td>
	<td align="right">563</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">4,127</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">3,186</td>
	<td align="right">8.96%</td>
	<td align="right">342</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,528</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">3,267</td>
	<td align="right">13.28%</td>
	<td align="right">537</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,804</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">2,200</td>
	<td align="right">11.57%</td>
	<td align="right">529</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,729</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">2,291</td>
	<td align="right">11.28%</td>
	<td align="right">490</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,781</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">2,387</td>
	<td align="right">11.24%</td>
	<td align="right">673</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,060</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">2,016</td>
	<td align="right">12.39%</td>
	<td align="right">387</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,403</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">1,169</td>
	<td align="right">11.57%</td>
	<td align="right">300</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,469</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">1</td>
	<td align="right">2.33%</td>
	<td align="right">107</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">108</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">15</td>
	<td align="right">2.74%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">39</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
