<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WFOR Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WFOR Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wfor/app/">Premium App</a>
| <a href="/wfor/iapp/">iPhone App</a>
| <a href="/wfor/wap/">Mobile Web</a>
| <a href="/wfor/sms/">SMS Usage</a>
| <a href="/wfor/video.php">Video Views</a>
| <a href="/wfor/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">535</td>
	<td align="right">1.84%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">538</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">517</td>
	<td align="right">1.41%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">530</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">695</td>
	<td align="right">1.85%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">739</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">507</td>
	<td align="right">1.63%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">527</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">845</td>
	<td align="right">2.55%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">863</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">862</td>
	<td align="right">2.41%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">882</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">2,074</td>
	<td align="right">2.98%</td>
	<td align="right">58</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,132</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">2,430</td>
	<td align="right">4.24%</td>
	<td align="right">67</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,497</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,272</td>
	<td align="right">3.26%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,295</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">1,181</td>
	<td align="right">3.32%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,185</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">1,179</td>
	<td align="right">3.24%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,202</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">1,009</td>
	<td align="right">3.00%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,030</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">1,013</td>
	<td align="right">3.01%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,035</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">1,015</td>
	<td align="right">3.29%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,050</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">1,037</td>
	<td align="right">3.09%</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,090</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">1,302</td>
	<td align="right">4.46%</td>
	<td align="right">102</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,404</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">1,443</td>
	<td align="right">5.48%</td>
	<td align="right">74</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,517</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">1,459</td>
	<td align="right">4.74%</td>
	<td align="right">108</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,567</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">1,529</td>
	<td align="right">5.79%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,592</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">1,415</td>
	<td align="right">4.79%</td>
	<td align="right">111</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,526</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,865</td>
	<td align="right">3.06%</td>
	<td align="right">176</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,041</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">4,351</td>
	<td align="right">6.41%</td>
	<td align="right">163</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">4,514</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,821</td>
	<td align="right">5.19%</td>
	<td align="right">122</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,943</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,736</td>
	<td align="right">5.36%</td>
	<td align="right">156</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,892</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">2,239</td>
	<td align="right">6.94%</td>
	<td align="right">156</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,395</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">2,215</td>
	<td align="right">6.22%</td>
	<td align="right">181</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,396</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,709</td>
	<td align="right">4.76%</td>
	<td align="right">110</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,819</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,824</td>
	<td align="right">5.13%</td>
	<td align="right">135</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,959</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">1,998</td>
	<td align="right">8.12%</td>
	<td align="right">166</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,164</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">1,518</td>
	<td align="right">7.99%</td>
	<td align="right">194</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,712</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">1,515</td>
	<td align="right">7.46%</td>
	<td align="right">256</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,771</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">2,051</td>
	<td align="right">9.66%</td>
	<td align="right">347</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,398</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">1,171</td>
	<td align="right">7.20%</td>
	<td align="right">165</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,336</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">999</td>
	<td align="right">9.89%</td>
	<td align="right">226</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,225</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">36</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">15</td>
	<td align="right">2.74%</td>
	<td align="right">66</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">81</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">8</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
