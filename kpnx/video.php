<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KPNX Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KPNX Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kpnx/app/">Premium App</a>
| <a href="/kpnx/iapp/">iPhone App</a>
| <a href="/kpnx/wap/">Mobile Web</a>
| <a href="/kpnx/sms/">SMS Usage</a>
| <a href="/kpnx/video.php">Video Views</a>
| <a href="/kpnx/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">102</td>
	<td align="right">0.35%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">105</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">187</td>
	<td align="right">0.51%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">189</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">117</td>
	<td align="right">0.31%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">126</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">104</td>
	<td align="right">0.33%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">112</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">126</td>
	<td align="right">0.38%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">129</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">81</td>
	<td align="right">0.23%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">89</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">281</td>
	<td align="right">0.40%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">297</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">328</td>
	<td align="right">0.57%</td>
	<td align="right">111</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">439</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">190</td>
	<td align="right">0.49%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">233</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">268</td>
	<td align="right">0.75%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">306</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">352</td>
	<td align="right">0.97%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">363</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">183</td>
	<td align="right">0.54%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">219</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">360</td>
	<td align="right">1.07%</td>
	<td align="right">191</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">551</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">402</td>
	<td align="right">1.30%</td>
	<td align="right">164</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">566</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">604</td>
	<td align="right">1.80%</td>
	<td align="right">232</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">836</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">519</td>
	<td align="right">1.78%</td>
	<td align="right">166</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">685</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">724</td>
	<td align="right">2.75%</td>
	<td align="right">158</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">882</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">563</td>
	<td align="right">1.83%</td>
	<td align="right">189</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">752</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">546</td>
	<td align="right">2.07%</td>
	<td align="right">175</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">721</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">782</td>
	<td align="right">2.65%</td>
	<td align="right">212</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">994</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,670</td>
	<td align="right">2.74%</td>
	<td align="right">238</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,908</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">701</td>
	<td align="right">1.03%</td>
	<td align="right">202</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">903</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">790</td>
	<td align="right">2.25%</td>
	<td align="right">209</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">999</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">730</td>
	<td align="right">2.25%</td>
	<td align="right">196</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">926</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">785</td>
	<td align="right">2.43%</td>
	<td align="right">55</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">840</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,033</td>
	<td align="right">2.90%</td>
	<td align="right">117</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,150</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">812</td>
	<td align="right">2.26%</td>
	<td align="right">236</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,048</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">4</td>
	<td align="right">0.01%</td>
	<td align="right">72</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">76</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">22</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
