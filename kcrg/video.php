<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KCRG Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KCRG Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kcrg/app/">Premium App</a>
| <a href="/kcrg/iapp/">iPhone App</a>
| <a href="/kcrg/wap/">Mobile Web</a>
| <a href="/kcrg/sms/">SMS Usage</a>
| <a href="/kcrg/video.php">Video Views</a>
| <a href="/kcrg/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">35</td>
	<td align="right">0.12%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">39</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">93</td>
	<td align="right">0.25%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">95</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">101</td>
	<td align="right">0.27%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">102</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">133</td>
	<td align="right">0.43%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">135</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">177</td>
	<td align="right">0.53%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">185</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">71</td>
	<td align="right">0.20%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">78</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">256</td>
	<td align="right">0.37%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">259</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">198</td>
	<td align="right">0.35%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">206</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">132</td>
	<td align="right">0.34%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">136</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">228</td>
	<td align="right">0.64%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">230</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">126</td>
	<td align="right">0.35%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">132</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">121</td>
	<td align="right">0.36%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">122</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">136</td>
	<td align="right">0.40%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">136</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">85</td>
	<td align="right">0.28%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">85</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">73</td>
	<td align="right">0.22%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">73</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">156</td>
	<td align="right">0.53%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">156</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">110</td>
	<td align="right">0.42%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">116</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">134</td>
	<td align="right">0.44%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">134</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">103</td>
	<td align="right">0.39%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">103</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">91</td>
	<td align="right">0.31%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">93</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">537</td>
	<td align="right">0.88%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">544</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">210</td>
	<td align="right">0.31%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">216</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">127</td>
	<td align="right">0.36%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">127</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">208</td>
	<td align="right">0.64%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">217</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">203</td>
	<td align="right">0.63%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">215</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">339</td>
	<td align="right">0.95%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">364</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">492</td>
	<td align="right">1.37%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">540</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">3</td>
	<td align="right">0.01%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">37</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
