<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KMTR Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KMTR Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kmtr/app/">Premium App</a>
| <a href="/kmtr/iapp/">iPhone App</a>
| <a href="/kmtr/wap/">Mobile Web</a>
| <a href="/kmtr/sms/">SMS Usage</a>
| <a href="/kmtr/video.php">Video Views</a>
| <a href="/kmtr/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">221</td>
	<td align="right">0.76%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">223</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">310</td>
	<td align="right">0.84%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">310</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">309</td>
	<td align="right">0.82%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">310</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">225</td>
	<td align="right">0.72%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">225</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">219</td>
	<td align="right">0.66%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">220</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">99</td>
	<td align="right">0.28%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">112</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">4</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
