<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KOB Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KOB Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kob/app/">Premium App</a>
| <a href="/kob/iapp/">iPhone App</a>
| <a href="/kob/wap/">Mobile Web</a>
| <a href="/kob/sms/">SMS Usage</a>
| <a href="/kob/video.php">Video Views</a>
| <a href="/kob/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">111</td>
	<td align="right">0.38%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">115</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">218</td>
	<td align="right">0.59%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">233</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">261</td>
	<td align="right">0.69%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">276</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">356</td>
	<td align="right">1.15%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">360</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">186</td>
	<td align="right">0.56%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">216</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">326</td>
	<td align="right">0.91%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">339</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">457</td>
	<td align="right">0.66%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">484</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">605</td>
	<td align="right">1.05%</td>
	<td align="right">71</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">676</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">478</td>
	<td align="right">1.22%</td>
	<td align="right">79</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">603</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">397</td>
	<td align="right">1.12%</td>
	<td align="right">74</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">477</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">561</td>
	<td align="right">1.54%</td>
	<td align="right">161</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">723</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">781</td>
	<td align="right">2.32%</td>
	<td align="right">136</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">923</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">672</td>
	<td align="right">2.00%</td>
	<td align="right">241</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">913</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">572</td>
	<td align="right">1.85%</td>
	<td align="right">200</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">775</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">756</td>
	<td align="right">2.25%</td>
	<td align="right">157</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">915</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">709</td>
	<td align="right">2.43%</td>
	<td align="right">216</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">925</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">668</td>
	<td align="right">2.54%</td>
	<td align="right">108</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">792</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">648</td>
	<td align="right">2.10%</td>
	<td align="right">272</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">936</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
