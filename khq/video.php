<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KHQ Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KHQ Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/khq/app/">Premium App</a>
| <a href="/khq/iapp/">iPhone App</a>
| <a href="/khq/wap/">Mobile Web</a>
| <a href="/khq/sms/">SMS Usage</a>
| <a href="/khq/video.php">Video Views</a>
| <a href="/khq/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">62</td>
	<td align="right">0.21%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">64</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">59</td>
	<td align="right">0.16%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">59</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">88</td>
	<td align="right">0.23%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">88</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">81</td>
	<td align="right">0.26%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">81</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">86</td>
	<td align="right">0.26%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">86</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">48</td>
	<td align="right">0.13%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">49</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">180</td>
	<td align="right">0.26%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">181</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">333</td>
	<td align="right">0.58%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">357</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">132</td>
	<td align="right">0.34%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">132</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">98</td>
	<td align="right">0.28%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">98</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">119</td>
	<td align="right">0.33%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">121</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">89</td>
	<td align="right">0.26%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">89</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">89</td>
	<td align="right">0.26%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">89</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">70</td>
	<td align="right">0.23%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">79</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">94</td>
	<td align="right">0.28%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">96</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">86</td>
	<td align="right">0.29%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">91</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">56</td>
	<td align="right">0.21%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">57</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">49</td>
	<td align="right">0.16%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">50</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">38</td>
	<td align="right">0.14%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">42</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">61</td>
	<td align="right">0.21%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">62</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">791</td>
	<td align="right">1.30%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">793</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">3,455</td>
	<td align="right">5.09%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,463</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">557</td>
	<td align="right">1.59%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">557</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">175</td>
	<td align="right">0.54%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">182</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">290</td>
	<td align="right">0.90%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">302</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">305</td>
	<td align="right">0.86%</td>
	<td align="right">54</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">359</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">4</td>
	<td align="right">0.01%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">23</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
