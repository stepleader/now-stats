<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KGPE Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KGPE Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kgpe/app/">Premium App</a>
| <a href="/kgpe/iapp/">iPhone App</a>
| <a href="/kgpe/wap/">Mobile Web</a>
| <a href="/kgpe/sms/">SMS Usage</a>
| <a href="/kgpe/video.php">Video Views</a>
| <a href="/kgpe/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">228</td>
	<td align="right">0.79%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">232</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">244</td>
	<td align="right">0.66%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">244</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">282</td>
	<td align="right">0.75%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">289</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">211</td>
	<td align="right">0.68%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">212</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">221</td>
	<td align="right">0.67%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">237</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">337</td>
	<td align="right">0.94%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">367</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">439</td>
	<td align="right">0.63%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">448</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">444</td>
	<td align="right">0.77%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">487</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">275</td>
	<td align="right">0.70%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">286</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">210</td>
	<td align="right">0.59%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">213</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">241</td>
	<td align="right">0.66%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">251</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">244</td>
	<td align="right">0.73%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">246</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">258</td>
	<td align="right">0.77%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">298</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">202</td>
	<td align="right">0.65%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">218</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">61</td>
	<td align="right">0.18%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">91</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">10</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
