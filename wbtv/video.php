<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WBTV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WBTV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wbtv/app/">Premium App</a>
| <a href="/wbtv/iapp/">iPhone App</a>
| <a href="/wbtv/wap/">Mobile Web</a>
| <a href="/wbtv/sms/">SMS Usage</a>
| <a href="/wbtv/video.php">Video Views</a>
| <a href="/wbtv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">229</td>
	<td align="right">0.79%</td>
	<td align="right">3</td>
	<td align="right">98</td>
	<td align="right">0</td>
	<td align="right">330</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">284</td>
	<td align="right">0.77%</td>
	<td align="right">11</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">315</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">278</td>
	<td align="right">0.74%</td>
	<td align="right">20</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">309</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">280</td>
	<td align="right">0.90%</td>
	<td align="right">11</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">307</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">369</td>
	<td align="right">1.11%</td>
	<td align="right">4</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">403</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">396</td>
	<td align="right">1.11%</td>
	<td align="right">8</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">435</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">730</td>
	<td align="right">1.05%</td>
	<td align="right">39</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">794</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">484</td>
	<td align="right">0.84%</td>
	<td align="right">49</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">555</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">475</td>
	<td align="right">1.22%</td>
	<td align="right">20</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">496</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">382</td>
	<td align="right">1.08%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">400</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">368</td>
	<td align="right">1.01%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">385</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">509</td>
	<td align="right">1.51%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">572</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">545</td>
	<td align="right">1.62%</td>
	<td align="right">56</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">601</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">531</td>
	<td align="right">1.72%</td>
	<td align="right">96</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">627</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">651</td>
	<td align="right">1.94%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">737</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">427</td>
	<td align="right">1.46%</td>
	<td align="right">120</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">547</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">333</td>
	<td align="right">1.26%</td>
	<td align="right">127</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">460</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">484</td>
	<td align="right">1.57%</td>
	<td align="right">159</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">646</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">397</td>
	<td align="right">1.50%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">466</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">447</td>
	<td align="right">1.51%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">511</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">934</td>
	<td align="right">1.53%</td>
	<td align="right">119</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,053</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">685</td>
	<td align="right">1.01%</td>
	<td align="right">77</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">762</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">458</td>
	<td align="right">1.30%</td>
	<td align="right">59</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">517</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">468</td>
	<td align="right">1.44%</td>
	<td align="right">92</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">560</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">517</td>
	<td align="right">1.60%</td>
	<td align="right">56</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">573</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">536</td>
	<td align="right">1.51%</td>
	<td align="right">47</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">583</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">759</td>
	<td align="right">2.11%</td>
	<td align="right">110</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">869</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">932</td>
	<td align="right">2.62%</td>
	<td align="right">52</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">984</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">635</td>
	<td align="right">2.58%</td>
	<td align="right">54</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">689</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">568</td>
	<td align="right">2.99%</td>
	<td align="right">133</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">701</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">606</td>
	<td align="right">2.98%</td>
	<td align="right">85</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">691</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">483</td>
	<td align="right">2.27%</td>
	<td align="right">70</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">553</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">7</td>
	<td align="right">0.07%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">7</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
