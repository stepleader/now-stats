<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KEYE Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KEYE Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/keye/app/">Premium App</a>
| <a href="/keye/iapp/">iPhone App</a>
| <a href="/keye/wap/">Mobile Web</a>
| <a href="/keye/sms/">SMS Usage</a>
| <a href="/keye/video.php">Video Views</a>
| <a href="/keye/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">196</td>
	<td align="right">0.68%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">206</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">182</td>
	<td align="right">0.50%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">187</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">339</td>
	<td align="right">0.90%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">341</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">234</td>
	<td align="right">0.75%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">242</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">268</td>
	<td align="right">0.81%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">278</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">313</td>
	<td align="right">0.88%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">336</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">2,969</td>
	<td align="right">4.27%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,987</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">850</td>
	<td align="right">1.48%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">879</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,159</td>
	<td align="right">2.97%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,165</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">487</td>
	<td align="right">1.37%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">530</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">770</td>
	<td align="right">2.12%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">785</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">657</td>
	<td align="right">1.95%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">665</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">640</td>
	<td align="right">1.90%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">672</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">725</td>
	<td align="right">2.35%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">737</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">519</td>
	<td align="right">1.55%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">541</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">484</td>
	<td align="right">1.66%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">502</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">627</td>
	<td align="right">2.38%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">647</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">526</td>
	<td align="right">1.71%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">556</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">599</td>
	<td align="right">2.27%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">624</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">829</td>
	<td align="right">2.80%</td>
	<td align="right">84</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">913</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">2,763</td>
	<td align="right">4.54%</td>
	<td align="right">41</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,804</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">6,044</td>
	<td align="right">8.90%</td>
	<td align="right">50</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">6,094</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,246</td>
	<td align="right">3.55%</td>
	<td align="right">136</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,382</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">816</td>
	<td align="right">2.52%</td>
	<td align="right">187</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,003</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">702</td>
	<td align="right">2.18%</td>
	<td align="right">259</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">961</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,075</td>
	<td align="right">3.02%</td>
	<td align="right">114</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,189</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,330</td>
	<td align="right">3.70%</td>
	<td align="right">335</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,665</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,305</td>
	<td align="right">3.67%</td>
	<td align="right">321</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,626</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">963</td>
	<td align="right">3.91%</td>
	<td align="right">310</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,273</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">744</td>
	<td align="right">3.91%</td>
	<td align="right">554</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,298</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">884</td>
	<td align="right">4.35%</td>
	<td align="right">305</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,189</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">1,095</td>
	<td align="right">5.16%</td>
	<td align="right">346</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,441</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">644</td>
	<td align="right">3.96%</td>
	<td align="right">493</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,137</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">502</td>
	<td align="right">4.97%</td>
	<td align="right">277</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">779</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">5</td>
	<td align="right">11.63%</td>
	<td align="right">197</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">202</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">94</td>
	<td align="right">17.18%</td>
	<td align="right">122</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">216</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">13</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
