<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KBCI Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KBCI Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kbci/app/">Premium App</a>
| <a href="/kbci/iapp/">iPhone App</a>
| <a href="/kbci/wap/">Mobile Web</a>
| <a href="/kbci/sms/">SMS Usage</a>
| <a href="/kbci/video.php">Video Views</a>
| <a href="/kbci/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">68</td>
	<td align="right">0.23%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">68</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">66</td>
	<td align="right">0.18%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">69</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">59</td>
	<td align="right">0.16%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">60</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">83</td>
	<td align="right">0.27%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">83</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">65</td>
	<td align="right">0.20%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">67</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">118</td>
	<td align="right">0.33%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">122</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">301</td>
	<td align="right">0.43%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">304</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">342</td>
	<td align="right">0.60%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">345</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">157</td>
	<td align="right">0.40%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">157</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">161</td>
	<td align="right">0.45%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">162</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">169</td>
	<td align="right">0.46%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">171</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">230</td>
	<td align="right">0.68%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">231</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">168</td>
	<td align="right">0.50%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">181</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">160</td>
	<td align="right">0.52%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">161</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">216</td>
	<td align="right">0.64%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">220</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">167</td>
	<td align="right">0.57%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">183</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">170</td>
	<td align="right">0.65%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">174</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">230</td>
	<td align="right">0.75%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">252</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">174</td>
	<td align="right">0.66%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">177</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">292</td>
	<td align="right">0.99%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">310</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">998</td>
	<td align="right">1.64%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,011</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">21</td>
	<td align="right">0.03%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">66</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
