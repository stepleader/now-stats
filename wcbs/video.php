<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WCBS Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WCBS Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wcbs/app/">Premium App</a>
| <a href="/wcbs/iapp/">iPhone App</a>
| <a href="/wcbs/wap/">Mobile Web</a>
| <a href="/wcbs/sms/">SMS Usage</a>
| <a href="/wcbs/video.php">Video Views</a>
| <a href="/wcbs/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">1,416</td>
	<td align="right">4.88%</td>
	<td align="right">52</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">1,469</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">2,253</td>
	<td align="right">6.14%</td>
	<td align="right">60</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">2,314</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">3,470</td>
	<td align="right">9.23%</td>
	<td align="right">116</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,586</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">2,093</td>
	<td align="right">6.73%</td>
	<td align="right">97</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,190</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">2,257</td>
	<td align="right">6.81%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,343</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">2,383</td>
	<td align="right">6.66%</td>
	<td align="right">83</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,466</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">3,320</td>
	<td align="right">4.77%</td>
	<td align="right">265</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,585</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">3,946</td>
	<td align="right">6.88%</td>
	<td align="right">190</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">4,136</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">3,206</td>
	<td align="right">8.21%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,226</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">2,789</td>
	<td align="right">7.85%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,829</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">3,551</td>
	<td align="right">9.77%</td>
	<td align="right">143</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,694</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">2,906</td>
	<td align="right">8.64%</td>
	<td align="right">90</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,996</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">4,080</td>
	<td align="right">12.14%</td>
	<td align="right">134</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">4,214</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">3,166</td>
	<td align="right">10.26%</td>
	<td align="right">96</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,262</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">2,889</td>
	<td align="right">8.62%</td>
	<td align="right">149</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,038</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">2,949</td>
	<td align="right">10.10%</td>
	<td align="right">170</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,119</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">2,767</td>
	<td align="right">10.51%</td>
	<td align="right">202</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,972</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">3,125</td>
	<td align="right">10.15%</td>
	<td align="right">187</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,312</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">2,692</td>
	<td align="right">10.20%</td>
	<td align="right">215</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,907</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">2,900</td>
	<td align="right">9.81%</td>
	<td align="right">227</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,127</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">3,276</td>
	<td align="right">5.38%</td>
	<td align="right">257</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,533</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">2,604</td>
	<td align="right">3.84%</td>
	<td align="right">149</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,753</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">2,817</td>
	<td align="right">8.03%</td>
	<td align="right">158</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,975</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">3,713</td>
	<td align="right">11.46%</td>
	<td align="right">324</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">4,037</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">3,384</td>
	<td align="right">10.50%</td>
	<td align="right">242</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,626</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">3,270</td>
	<td align="right">9.18%</td>
	<td align="right">185</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,455</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">2,698</td>
	<td align="right">7.51%</td>
	<td align="right">278</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,976</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">2,965</td>
	<td align="right">8.34%</td>
	<td align="right">345</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,310</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">2,831</td>
	<td align="right">11.50%</td>
	<td align="right">391</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,222</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">2,391</td>
	<td align="right">12.58%</td>
	<td align="right">830</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">3,221</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">2,413</td>
	<td align="right">11.88%</td>
	<td align="right">457</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,870</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">1,872</td>
	<td align="right">8.81%</td>
	<td align="right">505</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,377</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">1,500</td>
	<td align="right">9.22%</td>
	<td align="right">595</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,095</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">979</td>
	<td align="right">9.69%</td>
	<td align="right">328</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,307</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">1</td>
	<td align="right">2.33%</td>
	<td align="right">292</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">293</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">29</td>
	<td align="right">5.30%</td>
	<td align="right">151</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">180</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">25</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
