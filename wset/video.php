<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WSET Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WSET Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wset/app/">Premium App</a>
| <a href="/wset/iapp/">iPhone App</a>
| <a href="/wset/wap/">Mobile Web</a>
| <a href="/wset/sms/">SMS Usage</a>
| <a href="/wset/video.php">Video Views</a>
| <a href="/wset/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">220</td>
	<td align="right">0.76%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">220</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">268</td>
	<td align="right">0.73%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">271</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">319</td>
	<td align="right">0.85%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">325</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">197</td>
	<td align="right">0.63%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">202</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">276</td>
	<td align="right">0.83%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">280</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">233</td>
	<td align="right">0.65%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">238</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">397</td>
	<td align="right">0.57%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">409</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">594</td>
	<td align="right">1.04%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">598</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">334</td>
	<td align="right">0.86%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">334</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">331</td>
	<td align="right">0.93%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">333</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">337</td>
	<td align="right">0.93%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">341</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">358</td>
	<td align="right">1.06%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">365</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">358</td>
	<td align="right">1.07%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">367</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">338</td>
	<td align="right">1.10%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">342</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">276</td>
	<td align="right">0.82%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">282</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">251</td>
	<td align="right">0.86%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">255</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">108</td>
	<td align="right">0.41%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">120</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">119</td>
	<td align="right">0.39%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">124</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">129</td>
	<td align="right">0.49%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">129</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">120</td>
	<td align="right">0.41%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">124</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">670</td>
	<td align="right">1.10%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">690</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">250</td>
	<td align="right">0.37%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">257</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">241</td>
	<td align="right">0.69%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">255</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">212</td>
	<td align="right">0.65%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">244</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">192</td>
	<td align="right">0.60%</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">219</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">173</td>
	<td align="right">0.49%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">197</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
