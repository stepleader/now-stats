<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WANE Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WANE Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wane/app/">Premium App</a>
| <a href="/wane/iapp/">iPhone App</a>
| <a href="/wane/wap/">Mobile Web</a>
| <a href="/wane/sms/">SMS Usage</a>
| <a href="/wane/video.php">Video Views</a>
| <a href="/wane/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">38</td>
	<td align="right">0.13%</td>
	<td align="right">0</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">70</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">62</td>
	<td align="right">0.17%</td>
	<td align="right">0</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">84</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">79</td>
	<td align="right">0.21%</td>
	<td align="right">0</td>
	<td align="right">60</td>
	<td align="right">0</td>
	<td align="right">139</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">69</td>
	<td align="right">0.22%</td>
	<td align="right">1</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">114</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">84</td>
	<td align="right">0.25%</td>
	<td align="right">2</td>
	<td align="right">122</td>
	<td align="right">0</td>
	<td align="right">208</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">110</td>
	<td align="right">0.31%</td>
	<td align="right">5</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">193</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">288</td>
	<td align="right">0.41%</td>
	<td align="right">2</td>
	<td align="right">60</td>
	<td align="right">0</td>
	<td align="right">350</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">327</td>
	<td align="right">0.57%</td>
	<td align="right">7</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">412</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">116</td>
	<td align="right">0.30%</td>
	<td align="right">0</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">154</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">101</td>
	<td align="right">0.28%</td>
	<td align="right">0</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">142</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">126</td>
	<td align="right">0.35%</td>
	<td align="right">0</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">137</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">155</td>
	<td align="right">0.46%</td>
	<td align="right">0</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">182</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">104</td>
	<td align="right">0.31%</td>
	<td align="right">6</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">134</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">152</td>
	<td align="right">0.49%</td>
	<td align="right">13</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">185</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">188</td>
	<td align="right">0.56%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">189</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">103</td>
	<td align="right">0.35%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">114</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">132</td>
	<td align="right">0.50%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">142</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">124</td>
	<td align="right">0.40%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">143</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">30</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
