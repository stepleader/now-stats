<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KVBC Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KVBC Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kvbc/app/">Premium App</a>
| <a href="/kvbc/iapp/">iPhone App</a>
| <a href="/kvbc/wap/">Mobile Web</a>
| <a href="/kvbc/sms/">SMS Usage</a>
| <a href="/kvbc/video.php">Video Views</a>
| <a href="/kvbc/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">227</td>
	<td align="right">0.78%</td>
	<td align="right">7</td>
	<td align="right">83</td>
	<td align="right">0</td>
	<td align="right">317</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">386</td>
	<td align="right">1.05%</td>
	<td align="right">5</td>
	<td align="right">159</td>
	<td align="right">0</td>
	<td align="right">550</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">399</td>
	<td align="right">1.06%</td>
	<td align="right">2</td>
	<td align="right">138</td>
	<td align="right">0</td>
	<td align="right">539</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">358</td>
	<td align="right">1.15%</td>
	<td align="right">9</td>
	<td align="right">144</td>
	<td align="right">0</td>
	<td align="right">511</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">499</td>
	<td align="right">1.51%</td>
	<td align="right">6</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">566</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">556</td>
	<td align="right">1.55%</td>
	<td align="right">42</td>
	<td align="right">109</td>
	<td align="right">0</td>
	<td align="right">707</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">659</td>
	<td align="right">0.95%</td>
	<td align="right">40</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">752</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">585</td>
	<td align="right">1.02%</td>
	<td align="right">77</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">713</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">800</td>
	<td align="right">2.05%</td>
	<td align="right">22</td>
	<td align="right">100</td>
	<td align="right">0</td>
	<td align="right">922</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">754</td>
	<td align="right">2.12%</td>
	<td align="right">22</td>
	<td align="right">99</td>
	<td align="right">0</td>
	<td align="right">880</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">730</td>
	<td align="right">2.01%</td>
	<td align="right">30</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">838</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">719</td>
	<td align="right">2.14%</td>
	<td align="right">25</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">755</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">616</td>
	<td align="right">1.83%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">650</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">717</td>
	<td align="right">2.32%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">727</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">857</td>
	<td align="right">2.56%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">895</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">528</td>
	<td align="right">1.81%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">545</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">520</td>
	<td align="right">1.97%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">563</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">696</td>
	<td align="right">2.26%</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">757</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">386</td>
	<td align="right">1.46%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">419</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">50</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">50</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
