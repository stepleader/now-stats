<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WEWS Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WEWS Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wews/app/">Premium App</a>
| <a href="/wews/iapp/">iPhone App</a>
| <a href="/wews/wap/">Mobile Web</a>
| <a href="/wews/sms/">SMS Usage</a>
| <a href="/wews/video.php">Video Views</a>
| <a href="/wews/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">160</td>
	<td align="right">0.55%</td>
	<td align="right">2</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">205</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">218</td>
	<td align="right">0.59%</td>
	<td align="right">15</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">284</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">240</td>
	<td align="right">0.64%</td>
	<td align="right">14</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">305</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">244</td>
	<td align="right">0.79%</td>
	<td align="right">23</td>
	<td align="right">72</td>
	<td align="right">0</td>
	<td align="right">339</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">374</td>
	<td align="right">1.13%</td>
	<td align="right">19</td>
	<td align="right">41</td>
	<td align="right">0</td>
	<td align="right">434</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">666</td>
	<td align="right">1.86%</td>
	<td align="right">16</td>
	<td align="right">68</td>
	<td align="right">0</td>
	<td align="right">750</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">931</td>
	<td align="right">1.34%</td>
	<td align="right">27</td>
	<td align="right">54</td>
	<td align="right">0</td>
	<td align="right">1,012</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">112</td>
	<td align="right">0.20%</td>
	<td align="right">10</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">130</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
