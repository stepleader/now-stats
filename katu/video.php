<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KATU Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KATU Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/katu/app/">Premium App</a>
| <a href="/katu/iapp/">iPhone App</a>
| <a href="/katu/wap/">Mobile Web</a>
| <a href="/katu/sms/">SMS Usage</a>
| <a href="/katu/video.php">Video Views</a>
| <a href="/katu/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">345</td>
	<td align="right">1.19%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">345</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">393</td>
	<td align="right">1.07%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">396</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">442</td>
	<td align="right">1.18%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">462</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">215</td>
	<td align="right">0.69%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">232</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">244</td>
	<td align="right">0.74%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">258</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">275</td>
	<td align="right">0.77%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">288</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">437</td>
	<td align="right">0.63%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">451</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">434</td>
	<td align="right">0.76%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">455</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">335</td>
	<td align="right">0.86%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">339</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">263</td>
	<td align="right">0.74%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">272</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">446</td>
	<td align="right">1.23%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">450</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">277</td>
	<td align="right">0.82%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">277</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">122</td>
	<td align="right">0.36%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">125</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">231</td>
	<td align="right">0.75%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">238</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">407</td>
	<td align="right">1.21%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">421</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">305</td>
	<td align="right">1.04%</td>
	<td align="right">42</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">347</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">180</td>
	<td align="right">0.68%</td>
	<td align="right">89</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">269</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">277</td>
	<td align="right">0.90%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">322</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">294</td>
	<td align="right">1.11%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">337</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">346</td>
	<td align="right">1.17%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">379</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">838</td>
	<td align="right">1.38%</td>
	<td align="right">82</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">928</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">86</td>
	<td align="right">0.13%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">116</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
