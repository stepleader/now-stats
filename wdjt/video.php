<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WDJT Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WDJT Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wdjt/app/">Premium App</a>
| <a href="/wdjt/iapp/">iPhone App</a>
| <a href="/wdjt/wap/">Mobile Web</a>
| <a href="/wdjt/sms/">SMS Usage</a>
| <a href="/wdjt/video.php">Video Views</a>
| <a href="/wdjt/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">245</td>
	<td align="right">0.84%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">250</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">454</td>
	<td align="right">1.24%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">474</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">387</td>
	<td align="right">1.03%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">391</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">316</td>
	<td align="right">1.02%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">331</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">360</td>
	<td align="right">1.09%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">394</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">759</td>
	<td align="right">2.12%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">771</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">869</td>
	<td align="right">1.25%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">895</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">412</td>
	<td align="right">0.72%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">469</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">433</td>
	<td align="right">1.11%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">435</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">502</td>
	<td align="right">1.41%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">510</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">341</td>
	<td align="right">0.94%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">350</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">362</td>
	<td align="right">1.08%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">367</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">490</td>
	<td align="right">1.46%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">502</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">365</td>
	<td align="right">1.18%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">367</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">302</td>
	<td align="right">0.90%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">304</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">140</td>
	<td align="right">0.48%</td>
	<td align="right">112</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">252</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">18</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
