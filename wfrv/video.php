<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WFRV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WFRV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wfrv/app/">Premium App</a>
| <a href="/wfrv/iapp/">iPhone App</a>
| <a href="/wfrv/wap/">Mobile Web</a>
| <a href="/wfrv/sms/">SMS Usage</a>
| <a href="/wfrv/video.php">Video Views</a>
| <a href="/wfrv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">40</td>
	<td align="right">0.14%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">43</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">79</td>
	<td align="right">0.22%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">96</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">92</td>
	<td align="right">0.24%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">93</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">53</td>
	<td align="right">0.17%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">61</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">88</td>
	<td align="right">0.27%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">91</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">256</td>
	<td align="right">0.72%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">259</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">483</td>
	<td align="right">0.69%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">487</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">414</td>
	<td align="right">0.72%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">427</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">142</td>
	<td align="right">0.36%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">161</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">190</td>
	<td align="right">0.53%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">193</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">126</td>
	<td align="right">0.35%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">126</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">115</td>
	<td align="right">0.34%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">120</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">125</td>
	<td align="right">0.37%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">129</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">73</td>
	<td align="right">0.24%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">82</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">197</td>
	<td align="right">0.59%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">207</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">207</td>
	<td align="right">0.71%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">213</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">135</td>
	<td align="right">0.51%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">136</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">101</td>
	<td align="right">0.33%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">114</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">129</td>
	<td align="right">0.49%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">152</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">153</td>
	<td align="right">0.52%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">239</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">818</td>
	<td align="right">1.34%</td>
	<td align="right">122</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">940</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">382</td>
	<td align="right">0.56%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">390</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">199</td>
	<td align="right">0.57%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">200</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">162</td>
	<td align="right">0.50%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">186</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">208</td>
	<td align="right">0.65%</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">239</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">376</td>
	<td align="right">1.06%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">376</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">577</td>
	<td align="right">1.61%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">602</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">638</td>
	<td align="right">1.79%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">666</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">220</td>
	<td align="right">0.89%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">240</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">66</td>
	<td align="right">0.35%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">80</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">181</td>
	<td align="right">0.89%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">202</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">168</td>
	<td align="right">0.79%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">186</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">20</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">46</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">17</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">30</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
