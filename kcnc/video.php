<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KCNC Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KCNC Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kcnc/app/">Premium App</a>
| <a href="/kcnc/iapp/">iPhone App</a>
| <a href="/kcnc/wap/">Mobile Web</a>
| <a href="/kcnc/sms/">SMS Usage</a>
| <a href="/kcnc/video.php">Video Views</a>
| <a href="/kcnc/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">140</td>
	<td align="right">0.48%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">147</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">269</td>
	<td align="right">0.73%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">284</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">212</td>
	<td align="right">0.56%</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">236</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">261</td>
	<td align="right">0.84%</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">286</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">257</td>
	<td align="right">0.78%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">263</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">501</td>
	<td align="right">1.40%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">549</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,090</td>
	<td align="right">1.57%</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,129</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">965</td>
	<td align="right">1.68%</td>
	<td align="right">87</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,052</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">694</td>
	<td align="right">1.78%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">708</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">606</td>
	<td align="right">1.71%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">634</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">1,116</td>
	<td align="right">3.07%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,130</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">680</td>
	<td align="right">2.02%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">700</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">441</td>
	<td align="right">1.31%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">470</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">306</td>
	<td align="right">0.99%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">315</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">408</td>
	<td align="right">1.22%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">413</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">531</td>
	<td align="right">1.82%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">563</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">397</td>
	<td align="right">1.51%</td>
	<td align="right">43</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">440</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">717</td>
	<td align="right">2.33%</td>
	<td align="right">112</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">831</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">346</td>
	<td align="right">1.31%</td>
	<td align="right">101</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">447</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">385</td>
	<td align="right">1.30%</td>
	<td align="right">83</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">468</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,620</td>
	<td align="right">2.66%</td>
	<td align="right">100</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,720</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">697</td>
	<td align="right">1.03%</td>
	<td align="right">92</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">789</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">610</td>
	<td align="right">1.74%</td>
	<td align="right">75</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">685</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">519</td>
	<td align="right">1.60%</td>
	<td align="right">81</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">600</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">505</td>
	<td align="right">1.57%</td>
	<td align="right">98</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">603</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">767</td>
	<td align="right">2.15%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">830</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,206</td>
	<td align="right">3.36%</td>
	<td align="right">107</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,313</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,308</td>
	<td align="right">3.68%</td>
	<td align="right">81</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,389</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">511</td>
	<td align="right">2.08%</td>
	<td align="right">173</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">684</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">470</td>
	<td align="right">2.47%</td>
	<td align="right">71</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">541</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">865</td>
	<td align="right">4.26%</td>
	<td align="right">51</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">916</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">805</td>
	<td align="right">3.79%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">868</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">491</td>
	<td align="right">3.02%</td>
	<td align="right">113</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">604</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">281</td>
	<td align="right">2.78%</td>
	<td align="right">119</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">400</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">48</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">16</td>
	<td align="right">2.93%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">45</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">70</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">70</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
