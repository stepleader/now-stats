<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WBRZ Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WBRZ Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wbrz/app/">Premium App</a>
| <a href="/wbrz/iapp/">iPhone App</a>
| <a href="/wbrz/wap/">Mobile Web</a>
| <a href="/wbrz/sms/">SMS Usage</a>
| <a href="/wbrz/video.php">Video Views</a>
| <a href="/wbrz/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">214</td>
	<td align="right">0.74%</td>
	<td align="right">5</td>
	<td align="right">109</td>
	<td align="right">0</td>
	<td align="right">328</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">428</td>
	<td align="right">1.17%</td>
	<td align="right">2</td>
	<td align="right">170</td>
	<td align="right">0</td>
	<td align="right">600</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">232</td>
	<td align="right">0.62%</td>
	<td align="right">18</td>
	<td align="right">134</td>
	<td align="right">0</td>
	<td align="right">384</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">269</td>
	<td align="right">0.87%</td>
	<td align="right">10</td>
	<td align="right">171</td>
	<td align="right">0</td>
	<td align="right">450</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">211</td>
	<td align="right">0.64%</td>
	<td align="right">15</td>
	<td align="right">105</td>
	<td align="right">0</td>
	<td align="right">331</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">154</td>
	<td align="right">0.43%</td>
	<td align="right">70</td>
	<td align="right">100</td>
	<td align="right">0</td>
	<td align="right">324</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">2,814</td>
	<td align="right">4.05%</td>
	<td align="right">39</td>
	<td align="right">444</td>
	<td align="right">0</td>
	<td align="right">3,297</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,989</td>
	<td align="right">3.47%</td>
	<td align="right">56</td>
	<td align="right">196</td>
	<td align="right">0</td>
	<td align="right">2,241</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">384</td>
	<td align="right">0.98%</td>
	<td align="right">0</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">453</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">263</td>
	<td align="right">0.74%</td>
	<td align="right">14</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">321</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">33</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">41</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
