<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>NOW Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>NOW Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/now/app/">Premium App</a>
| <a href="/now/iapp/">iPhone App</a>
| <a href="/now/wap/">Mobile Web</a>
| <a href="/now/sms/">SMS Usage</a>
| <a href="/now/video.php">Video Views</a>
| <a href="/now/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th colspan="1">
		Month
	</th>
	<th colspan="1">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		WAP
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">29,019</td>
	<td align="right">658</td>
	<td align="right">5,925</td>
	<td align="right">585</td>
	<td align="right">36,192</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">36,702</td>
	<td align="right">874</td>
	<td align="right">5,846</td>
	<td align="right">153</td>
	<td align="right">43,575</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">37,599</td>
	<td align="right">1,152</td>
	<td align="right">6,585</td>
	<td align="right">608</td>
	<td align="right">45,944</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">31,081</td>
	<td align="right">1,533</td>
	<td align="right">6,575</td>
	<td align="right">168</td>
	<td align="right">39,357</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">33,146</td>
	<td align="right">1,459</td>
	<td align="right">3,892</td>
	<td align="right">516</td>
	<td align="right">39,013</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">35,763</td>
	<td align="right">2,749</td>
	<td align="right">4,106</td>
	<td align="right">123</td>
	<td align="right">42,766</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">69,540</td>
	<td align="right">3,102</td>
	<td align="right">4,355</td>
	<td align="right">584</td>
	<td align="right">77,581</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">57,359</td>
	<td align="right">3,572</td>
	<td align="right">3,136</td>
	<td align="right">82</td>
	<td align="right">64,175</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">39,042</td>
	<td align="right">1,294</td>
	<td align="right">3,633</td>
	<td align="right">136</td>
	<td align="right">44,262</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">35,526</td>
	<td align="right">1,288</td>
	<td align="right">3,490</td>
	<td align="right">72</td>
	<td align="right">40,487</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">36,357</td>
	<td align="right">1,847</td>
	<td align="right">2,528</td>
	<td align="right">67</td>
	<td align="right">40,803</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">33,626</td>
	<td align="right">2,029</td>
	<td align="right">1,502</td>
	<td align="right">113</td>
	<td align="right">37,277</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">33,606</td>
	<td align="right">2,580</td>
	<td align="right">1,442</td>
	<td align="right">144</td>
	<td align="right">37,776</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">30,864</td>
	<td align="right">2,329</td>
	<td align="right">1,817</td>
	<td align="right">86</td>
	<td align="right">35,111</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">33,527</td>
	<td align="right">2,775</td>
	<td align="right">641</td>
	<td align="right">144</td>
	<td align="right">37,138</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">29,202</td>
	<td align="right">2,898</td>
	<td align="right">416</td>
	<td align="right">304</td>
	<td align="right">32,820</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">26,338</td>
	<td align="right">2,651</td>
	<td align="right">299</td>
	<td align="right">91</td>
	<td align="right">29,642</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">30,800</td>
	<td align="right">3,837</td>
	<td align="right">334</td>
	<td align="right">0</td>
	<td align="right">35,165</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">26,399</td>
	<td align="right">2,901</td>
	<td align="right">346</td>
	<td align="right">0</td>
	<td align="right">29,671</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">29,558</td>
	<td align="right">4,272</td>
	<td align="right">162</td>
	<td align="right">0</td>
	<td align="right">34,044</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">60,892</td>
	<td align="right">4,417</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">65,355</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">67,891</td>
	<td align="right">3,486</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">71,377</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">35,098</td>
	<td align="right">3,007</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">38,105</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">32,393</td>
	<td align="right">4,059</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">36,452</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">32,243</td>
	<td align="right">4,082</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">36,326</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">35,613</td>
	<td align="right">3,727</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">39,340</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">35,934</td>
	<td align="right">4,485</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">40,419</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">35,555</td>
	<td align="right">3,593</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">39,148</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">24,607</td>
	<td align="right">4,337</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">28,944</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">19,009</td>
	<td align="right">5,456</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">24,465</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">20,312</td>
	<td align="right">4,496</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">24,808</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">21,240</td>
	<td align="right">4,827</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">26,067</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">16,266</td>
	<td align="right">4,324</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">20,590</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">10,103</td>
	<td align="right">3,300</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">13,403</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">43</td>
	<td align="right">2,015</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,058</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">547</td>
	<td align="right">1,547</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,094</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">3</td>
	<td align="right">690</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">693</td>
</tr>
<tr>
	<td>Feb. 2006</td>
	<td align="right">0</td>
	<td align="right">428</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">428</td>
</tr>
<tr>
	<td>Jan. 2006</td>
	<td align="right">0</td>
	<td align="right">397</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">397</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
