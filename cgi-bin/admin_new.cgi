#!/usr/bin/perl -w 
####---------------------------------------------------------------------------
####    Name: admin.cgi
####  Author: Albert Choy
####    Date: 07.28.2005
####
####  Web interface that allows affiliates to override the main story on the
####     first page of the wireless app.
####  This would be stuff like headline, text.
####  
####---------------------------------------------------------------------------
####  Modified: 4/28 by Tom R. 
####     * Cleaned up HTML
####     * Added better CSS support
####     * Added directions
####---------------------------------------------------------------------------
####  Shutdown: 12/01/2008 by Tom R.
####---------------------------------------------------------------------------

use strict;
my $AFFILIATE =find_afl();


print "Content-type: text/html\n\n";
print qq|
<HTML>
<HEAD>
<TITLE>News Over Wireless Administration</TITLE>
<link REL="stylesheet" TYPE="text/css" href="../style.css">
</HEAD>
<BODY BGCOLOR="#FFFFFF">

<TABLE BORDER="1" WIDTH="100%" CELLSPACING="0" CELLPADDING="5" BGCOLOR="#FFFFFF">
<TR>
<TD ALIGN="LEFT" VALIGN="MIDDLE" WIDTH=15%>
<IMG SRC="../images/logo-now.jpg"  BORDER=0>
</TD>
<TD ALIGN="CENTER" VALIGN="MIDDLE" WIDTH=100% BGCOLOR="#3399ff">
<span class="header">News Over Wireless</span>
</TD>
</TR>
</TABLE>
<BR>

<h2>Top Story Admin Tool Disabled</h2>
<p>As discussed in a recent Partner Portal posting, the top story administration tool has been disabled.</p>
<p>If you have questions or concerns, please contact News Over Wireless support at <a href="mailto:support\@newsoverwireless.com?subject=[$AFFILIATE] Top Story Tool">support\@newsoverwireless.com</a></p>

</body>
</html>

|;
exit;

# from the current directory, figure out the affiliate
sub find_afl {

    my $afl = `pwd`;

	my $lasttoken = '';
    if ($afl  =~ m#.+/(\S+?)$#) {
		$lasttoken = $1;
	}
    return uc($lasttoken);
}
