#!/usr/bin/perl -w

######################################################################
# NOW VIDEO VIEWERSHIP
######################################################################
# AUTHOR:  Tom Rouillard
# DATE:    August 11, 2006
# SCRIPT:  viewership.cgi
# PURPOSE: Present video views for each affiliate using stats data
#
######################################################################

use strict;
use Date::Format;
use Date::Manip;
use File::Slurp;
use File::Listing qw(parse_dir);
use CGI qw/:standard/;
use CGI::Pretty qw( :html3 );

my $q             = CGI->new();
my $header        = 0;
my $running_total = 0;
my $data_dir      = "/public/WebServer/cgi-bin/stats/data/";
my $report_dir    = "/public/nowstats/now/reports/";
my $script_url    = "/cgi-bin/viewership.cgi";
my $excludes      = "perm|pbs|wraz|wbhg|75\.55\.199*";

if ($q->param('action') eq "getDate") {
	my $date = $q->param('month') . $q->param('year');
	my $filename = "awstats" . $date . ".video.newsoverwireless.com.txt";
	my $form = create_form($data_dir);
	&print_report($filename, $form);
}

elsif ($q->param('action') eq "report") {
	my $date = $q->param('month') . $q->param('year');
	my $filename = "awstats" . $date . ".video.newsoverwireless.com.txt";
	my $form = create_form($data_dir);
	&create_report($filename, $form);
}

else {
	my $date = time2str("%m%Y", time);
	my $month = time2str("%m", time);
	my $year = time2str("%Y", time);
	
	my $filename = "awstats" . $date . ".video.newsoverwireless.com.txt";
	$q->param(-name=>'type', -value=>'All Video');
	$q->param(-name=>'month', -value=>$month);
	$q->param(-name=>'year', -value=>$year);
	my $form = create_form($data_dir);
	&print_report($filename, $form);
}

exit;


#################
## SUBROUTINES 
#################

sub print_report {
	my ($filename, $form) = @_;
	my @month;
	my %AFF = read_data($filename);
	
	$filename =~ /awstats(\d{2})(\d{4}).video.newsoverwireless.com.txt/;
	$month[4] = $1-1 ;
	my $date = strftime("%b", @month);
	$date = "$date. $2";
	
	my $report_url = $script_url . "?month=" . $q->param('month') . "&year=" . $q->param('year') . "&type=" . $q->param('type') . "&action=report";

	# reverse sort the hash based on values for printing later
	my @sorted = reverse sort { $AFF{$a} <=> $AFF{$b} } keys %AFF;

	# print the report
	&html_header();
	print $q->start_html(-title=>'NOW Video Stats',-style=>'/includes/stats.css');
	print $q->p($form);
	print $q->p( a({href=>$report_url}, "Create Report") );
	print h3("Affiliate Video Views for $date");
	print $q->start_table({-width=>'300', -align=>'left', -border=>'1'});
	print $q->Tr([th(['Affiliate','Views','Percent'])]);
	foreach my $key (@sorted) {
		my $aff_total   = &commify($AFF{$key});
		my $aff_name    = uc($key);
		my $aff_percent = sprintf("%.2f", (($AFF{$key}/$running_total) * 100));
		print Tr(
					td($aff_name),
					td({-align=>'right'}, $aff_total),
					td({-align=>'right'}, $aff_percent)
				);
	}
	
	my $pr_total = &commify($running_total);
	print $q->Tr(
					th({-align=>'left'}, 'Total'),
					th({-align=>'right'}, $pr_total),
					th({-align=>'right'}, '100'),
				);
	print $q->end_table();
	print $q->end_html();
}

sub create_report {
	my ($filename) = @_;
	my %AFF = read_data($filename);
	my $report = "Affiliate\tViews\n";
	my $x = 1;

	my @sorted = reverse sort { $AFF{$a} <=> $AFF{$b} } keys %AFF;
	foreach my $key (@sorted) {
		$x++;
		my $aff_total  = $AFF{$key};
		my $aff_name   = uc($key);
		$report       .= $aff_name . "\t" . $aff_total . "\n";
	}
	$report .= "Total\t" . "=sum(B2:B" . $x . ")\n";
	my $report_name = $q->param('type') . "-" . $q->param('year') . $q->param('month') . ".xls";
	$report_name =~ s/ /-/g;
	my $report_path = $report_dir . $report_name;
	write_file($report_path, $report);
	my $url = "http://stats.newsoverwireless.com/now/reports/$report_name";
	print "Location: $url\n\n";

}


sub read_data {
	my ($filename) = @_;
	if ($filename !~ /awstats\d{6}.video.newsoverwireless.com.txt/) {
		&error("Invalid filename!<p>$filename");
	}
	my $stats_path = $data_dir. $filename;
	my ($file, $total, $data);
	
	$file = read_file($stats_path) || &error("Couldn't open: $stats_path");
	
	# get the file report from the monthly stats
	while ($file =~ /BEGIN_SIDER (\d+)\n/) {      # find the beginning of content we want
		$total = $1;                              # set total count using stats file
		$data = $';                               # var contains everything after that line
		$file = $';                               # This ends the loop
		$data =~ /END_SIDER\n/;                   # find the end of the content we want
		$data = $`;                               # var contains everything before that line
	}
	
	my %AFF = &parse_data($data);  # retrieve parsed data in hash
	return (%AFF);

}

sub parse_data {
	my ($data) = @_;
	my %AFF; # we'll return this hash

	# Run through each line of the data looking for channel video
	# push the affiliate name into the hash key & add hits to hash
	my @lines = split(/\n/, $data);

	# find channel 58 video based on directories
	if ($q->param('type') eq "Channel 58") {
		foreach my $line(@lines) {
			next if ($line =~ /$excludes/i);  # don't process the previews!
			if (   ($line =~ m#^/(\D+)/ev/[^ ]+ (\d+).*$#)
				|| ($line =~ m#^/(\D+)/mcd/[^ ]+ (\d+).*$#)
				|| ($line =~ m#^/(\D+)/tr1/[^ ]+ (\d+).*$#)
				|| ($line =~ m#^/(\D+)/tr2/[^ ]+ (\d+).*$#)
			) {
				my $callsign = $1;
				my $hitcount = $2;
				if (!$AFF{$callsign}) {
					$AFF{$callsign} = $hitcount;
				}
				else {
					$AFF{$callsign} += $hitcount;
				}
				$running_total += $hitcount; # running_total is a global variable
			}
		}
	}

	# find java video based on directories
	elsif ($q->param('type') eq "Java Video") {
		foreach my $line(@lines) {
			next if ($line =~ /$excludes/i);  # don't process the previews!
			if (   ($line =~ m#^/(\D+)/hi/[^ ]+ (\d+).*$#)
				|| ($line =~ m#^/(\D+)/lo/[^ ]+ (\d+).*$#)
			) {
				my $callsign = $1;
				my $hitcount = $2;
				$callsign =~ s#storage/wral#wral#i; # phone news & wx are in /storage/wral
				if (!$AFF{$callsign}) {
					$AFF{$callsign} = $hitcount;
				}
				else {
					$AFF{$callsign} += $hitcount;
				}
				$running_total += $hitcount; # running_total is a global variable
			}
		}
	}

	# find all video
	else {
		foreach my $line(@lines) {
			next if ($line =~ /$excludes/i);  # don't process the previews!
			if ($line =~ m#^/(\D+)/[^/]+/[^ ]+ (\d+).*$#) {
				my $callsign = $1;
				my $hitcount = $2;
				$callsign =~ s#storage/wral#wral#i; # phone news & wx are in /storage/wral
				if (!$AFF{$callsign}) {
					$AFF{$callsign} = $hitcount;
				}
				else {
					$AFF{$callsign} += $hitcount;
				}
				$running_total += $hitcount; # running_total is a global variable
			}
		}
	}
	return (%AFF);
}

# subroutine to create the months pulldown menu based on data files
sub create_form {
	my ($data_dir) = @_;
	my $date_menu = &get_months($data_dir);

	my $form_html = $q->startform(-Method=>'get');
	$form_html .= $date_menu;
	$form_html .= $q->popup_menu(-name=>'type', -Values=>['Channel 58','Java Video','All Video']);
	$form_html .= $q->hidden(-name=>'action', -value=>'getDate');
	$form_html .= $q->submit(-name=>'submit', -value=>'Go');
	$form_html .= $q->endform();
	return ($form_html);
}

# subroutine to get the available months from data file listing
sub get_months {
	my ($path) = @_;
	my @mvalues;
	my %mlabels;
	my @yvalues;
	my %ylabels;
	my %years;

	chdir $path || &error("can't change directories: $path");

	foreach (1..12) {
		my $month_num = sprintf("%02s",$_);
		my $date      = ParseDateString("2007" . $month_num);
		my $month_nam = UnixDate($date,"%b");
		push (@mvalues, $month_num);
		$mlabels{$month_num} = $month_nam;
	}
	
	for (parse_dir(`ls -l`)) {
		my ($name, $type, $size, $mtime, $mode) = @$_;
		$name =~ m/^awstats(\d{2})(\d{4}).*/;
		$years{$2} = $2;
	 }

	foreach my $year (sort keys(%years)) {
		push (@yvalues, $year);
		$ylabels{$year} = $year;
	}
	@mvalues = sort (@mvalues);
	my $month_menu = popup_menu(-name=>'month', -values=>\@mvalues, -labels=>\%mlabels,);
	@yvalues = reverse (@yvalues);
	my $year_menu = popup_menu(-name=>'year', -values=>\@yvalues, -labels=>\%ylabels,);

	return ($month_menu . $year_menu);
}


# subroutine to track whether we printed the HTML header
sub html_header {
	if ($header < 1) {
		print $q->header();
		$header++
	}
}

# subroutine to add commas to numbers for pretty printing
sub commify {
    local($_) = shift;
    1 while s/^(-?\d+)(\d{3})/$1,$2/;
    return $_;
}

# subroutine for printing generic errors
sub error {
	my ($error) = @_;
	&html_header();
	print $q->start_html(-title=>'Error',-style=>'/includes/stats.css');
	print $q->h1({-align=>'center'}, 'Error');
	print $q->p({-align=>'center'}, $error);
	print $q->end_html();
	exit;
}
