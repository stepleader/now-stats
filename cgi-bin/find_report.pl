#!/usr/bin/perl -w
####-------------------------------------------
####   Name: find_report.pl
#### Author: John E. Clark
####   Date: 06.24.2005
####-------------------------------------------

use CGI qw(:standard);
use Time::Local;
use File::Slurp;
use strict;

#my $q = new CGI();

my ($day, $month, $year) = (localtime(time()))[3,4,5];
$month++;
$year   =  ($year > 69) ? $year + 1900 : $year + 2000;
$day    =~ s#^(\d)$#0$1#go;
$month  =~ s#^(\d)$#0$1#go;

my $affiliate = param('affiliate');
my $type      = param('type');

my $location = "http://stats.newsoverwireless.com/$affiliate/";
my $path;

if ($type eq "month") {
    my $date = param('date');
    my ($month, $day, $year) = ($date =~ m#(\d\d)/(\d\d)/(\d\d\d\d)#);

    $path = "$year/monthly_report_$year$month"."01.html";

}

elsif ($type eq "week") {
    my $date = param('date');
    my ($month, $day, $year) = ($date =~ m#(\d\d)/(\d\d)/(\d\d\d\d)#);

    #### Do the decrement of a month for timelocal
    $month = ($month - 1);
    if ($month < 0) {
        $month = 11;
        $year = ($year - 1);
    }

    my $unixtime = timelocal("0","0","0",$day,$month,$year);
    my ($wday) = (localtime($unixtime))[6];

    #### Take the current wday and multiply it by 86400 - that will get us back to the previous Sunday
    my $back = (86400 * $wday);
    my $timeback = ($unixtime - $back);

    my ($newday, $newmonth, $newyear) = (localtime($timeback))[3,4,5];
    $newmonth++;
    $newyear   =  ($newyear > 69) ? $newyear + 1900 : $newyear + 2000;
    $newday    =~ s#^(\d)$#0$1#go;
    $newmonth  =~ s#^(\d)$#0$1#go;

    $path = "$year/weekly_report_$newyear$newmonth$newday.html";
}
else {
    my $date = param('date');
    my ($month, $day, $year) = ($date =~ m#(\d\d)/(\d\d)/(\d\d\d\d)#);

    $path = "$year/daily_report_$year$month$day.html";
}

if (-e  "/public/nowstats/$affiliate/$path") {
	$location .= $path;
	print "Location: $location\n\n";
}
else {
	my $template = read_file("/public/nowstats/template.html");
	my $AFLT = uc($affiliate);
	my $message =qq|
<p>This report does not exist.</p>
<p><a href="http://stats.newsoverwireless.com/$affiliate/">Please return to the usage stats page.</a></p>
|;
	
	$template =~ s/\[AFFILIATE\] Mobile Usage Statistics/ERROR/g;
	$template =~ s/\[AFFILIATE\]/$AFLT/g;
	$template =~ s/\[REPORTNAME\]//g;
	$template =~ s/\[REPORTBODY\]/$message/g;
	$template =~ s/\[TIMESTAMP\]//g;

	print header();
	print $template;
}

exit;
