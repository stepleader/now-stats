#!/usr/bin/perl -w 

use CGI qw(:standard);
use LWP::Simple qw(!head);
use Date::Format;
use File::Slurp;
use strict;
use CGI::Carp qw(fatalsToBrowser);

my $q           = CGI->new();
my $header      = 0;
my %WRAPPERS;
set_wrappers();

if ($q->param('action') eq "Update") {
	&update_page();
}
else {
	&print_form("Update Templates");
}

exit;

sub print_form {
	my $header = shift();
	my @affiliates;
	while ( my ($key, $value) = each(%WRAPPERS) ) {
		push (@affiliates, $key);
	}
	@affiliates = sort(@affiliates);
	my $afflist = popup_menu(-name=>'affiliate', -values=>\@affiliates);
	&html_header();
	print $q->start_html(-title=>'CBS Template Test', -style=>'/includes/casl.css',);
	print $q->start_div({-id=>'Wrapper'});
	print $q->h2($header);
	print $q->startform();
	print $afflist;
	print $q->submit(-name=>'action', -value=>'Update');
	print $q->endform();
	print $q->end_html();
}

sub update_page {
	my $affiliate   = $q->param('affiliate');
	my $header_url  = $WRAPPERS{$affiliate};
	my $footer_url  = $WRAPPERS{$affiliate};
	$footer_url =~ s/header/footer/;
	
	my $header_path = "/public/affiliates/". $affiliate . "/header_test.html";
	my $footer_path = "/public/affiliates/". $affiliate . "/footer_test.html";
	
	my $header_html = get($header_url);
	my $footer_html = get($footer_url);
	
	if ( (!$header_html) || (!$footer_html) ) {
		error("Can't get header or footer from web.");
	}
#	elsif ( (!-e $header_path) || (!-e $footer_path) ) {
#		error("Can't find header or footer on file system.<br>$header_path<br>$footer_path");
#	}
	else {
		write_file( $header_path, $header_html ) || error("Can't write to header file");
		write_file( $footer_path, $footer_html ) || error("Can't write to footer file") ;
	}

	&html_header();
	print $q->start_html(-title=>'Template Test', -style=>'/includes/casl.css',);
	print $q->start_div({-id=>'Wrapper'});
	print $q->h1('Templates Updated');
	test_link($affiliate);

	print_form("Update Again");
}

# subroutine for tracking whether we printed the HTML header yet
sub html_header {
	if ($header < 1) {
		print $q->header();
		$header++
	}
}

sub test_link {
	my $affiliate = shift();
	print $q->p({-class=>'link'}, 'Check the ' . 
					a(
						{-href=>'http://' . $affiliate . '.newsoverwireless.com/designtest.php', 
						-target=>'new', 
						-title=>'Open in a new window'}, 
					'Test Page')
				);
}

sub set_wrappers {
	%WRAPPERS = (
		"wcbs" => "http://wcbstv.com/wrapper.aspx?output=header&sid=3940",
		"kcbs" => "http://cbs2.com/wrapper.aspx?output=header&sid=3931",
		"wbbm" => "http://cbs2chicago.com/wrapper.aspx?output=header&sid=3938",
		"kyw"  => "http://cbs3.com/wrapper.aspx?output=header&sid=3937",
		"wbz"  => "http://wbztv.com/wrapper.aspx?output=header&sid=3939",
		"kpix" => "http://cbs5.com/wrapper.aspx?output=header&sid=3935",
		"ktvt" => "http://cbs11tv.com/wrapper.aspx?output=header&sid=3936",
		"wcco" => "http://wcco.com/wrapper.aspx?output=header&sid=3941",
		"wfor" => "http://cbs4.com/wrapper.aspx?output=header&sid=3942",
		"kcnc" => "http://cbs4denver.com/wrapper.aspx?output=header&sid=3932",
		"kovr" => "http://cbs13.com/wrapper.aspx?output=header&sid=3934",
		"kdka" => "http://kdka.com/wrapper.aspx?output=header&sid=3933",
		"wjz"  => "http://wjz.com/wrapper.aspx?output=header&sid=3943",
	);
}

# subroutine for printing generic errors
sub error {
	my ($error) = @_;
	&html_header();
	print $q->start_html(-title=>'Error',-style=>'/includes/casl.css');
	print $q->h1({-align=>'center'}, 'Error');
	print $q->p({-align=>'center'}, $error);
	print $q->end_html();
	exit;
}
