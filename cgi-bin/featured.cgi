#!/usr/bin/perl

use strict;
use File::Slurp;
use DBI;
use CGI qw/:standard/;
use CGI::Pretty qw( :html3 );
use CGI::Cookie;
use CGI::Carp qw(fatalsToBrowser);
use Data::Dumper; #print "<pre>\n" . Dumper($alldata) . "\n</pre>\n";
use Date::Manip;
use POSIX qw(ceil floor);

my $header            = 0;
$CGI::DISABLE_UPLOADS = 1;       # No uploads allowed
$CGI::POST_MAX        = 102_400; # Limit requests to 100KB
my $q                 = new CGI;
my $script_url        = "/cgi-bin/featured.cgi";
my $db_conf           = read_file('/public/db_conf/featured.txt');
chomp $db_conf;
my ($un,$pw,$db,$host) = split(/:/,$db_conf);
my $dbh               = DBI->connect("DBI:mysql:$db:$host","$un","$pw");
my $action            = $q->param('action');
my $mode              = $q->param('mode');
my $method            = "get";
my $logout_link       = "<a href=\"$script_url?mode=main&action=Help\">Help</a> | <a href=\"$script_url?mode=main&action=Logout\">Logout</a> | <a href=\"$script_url\">Main</a> ";

&evaluate_action();
exit;

sub evaluate_action {
	my $id;
	my $firstname;
	my $lastname;

	if (!$mode) {
		($id, $firstname, $lastname) = &verify_session();
		&display_main_menu($id, $firstname, $lastname);
	}

	elsif ($mode eq "login") {
		if ($action eq "login") {
			&verify_login();
		}
		else {
			&error(60, $firstname, $lastname);
		}
	}

	elsif ($mode eq "main") {
		($id, $firstname, $lastname) = &verify_session();

		if ($action eq "Create") {
			&form_story($id, $firstname, $lastname);
		}
		elsif ($action eq "Manage") {
			&display_manage_menu($id, $firstname, $lastname);
		}
		elsif ($action eq "Front") {
			&display_manage_front($id, $firstname, $lastname);
		}
		elsif ($action eq "Account") {
			&form_edit_account($id, $firstname, $lastname);
		}
		elsif ($action eq "Logout") {
			&verify_logout($id);
		}
		elsif ($action eq "Help") {
			&display_help_text($id, $firstname, $lastname);
		}
		elsif ($action eq "Cancel") {
			&display_main_menu($id, $firstname, $lastname);
		}
		else {
			&error(60, $firstname, $lastname);
		}
	}

	elsif ($mode eq "manage") {
		($id, $firstname, $lastname) = &verify_session();

		if ($action eq "Find") {
			&form_story($id, $firstname, $lastname);
		}
		elsif ($action eq "Edit") {
			&form_story($id, $firstname, $lastname);
		}
		elsif ($action eq "Front") {
			&add_to_front($id, $firstname, $lastname);
		}
		elsif ($action eq "Delete") {
			&delete_story($id, $firstname, $lastname);
		}
		elsif ($action eq "Cancel") {
			&display_main_menu($id, $firstname, $lastname);
		}
		else {
			&error(60, $firstname, $lastname);
		}
	}

	elsif ($mode eq "front") {
		($id, $firstname, $lastname) = &verify_session();
		
		if ($action eq "Save") {
			&save_front($id, $firstname, $lastname);
		}
		elsif ($action eq "Cancel") {
			&display_main_menu($id, $firstname, $lastname);
		}
		else {
			&error(60, $firstname, $lastname);
		}
	}

	elsif ($mode eq "editor") {
		($id, $firstname, $lastname) = &verify_session();
		
		if ($action eq "Save") {
			&save_story($id, $firstname, $lastname);
		}
		elsif ($action eq "Cancel") {
			&display_main_menu($id, $firstname, $lastname);
		}
		else {
			&error(60, $firstname, $lastname);
		}
	}
	
	elsif ($mode eq "users") {
		($id, $firstname, $lastname) = &verify_session();

		if ($action eq "Cancel") {
			&display_main_menu($id, $firstname, $lastname);
		}
		elsif ($action eq "Save Account") {
			&save_account($id);
		}
		elsif ($action eq "Edit") {
			&edit_form($id, $firstname, $lastname);
		}
		else {
			&error(60, $firstname, $lastname);
		}
	}

	else {
		($id, $firstname, $lastname) = &verify_session();
		&error(60, $firstname, $lastname);
	}
}
