#!/usr/bin/perl -w 
####---------------------------------------------------------------------------
####    Name: admin.cgi
####  Author: Albert Choy
####    Date: 07.28.2005
####
####  Web interface that allows affiliates to override the main story on the
####     first page of the wireless app.
####  This would be stuff like headline, text.
####  
####---------------------------------------------------------------------------
####  Modified: 4/28 by Tom R. 
####     * Cleaned up HTML
####     * Added better CSS support
####     * Added directions
####---------------------------------------------------------------------------

use CGI qw(:standard);
use File::Path;
use File::Basename;
use FileHandle;
use DB_File;
use Net::FTP;
use strict;


my $AFFILIATE =find_afl();

my $text = "";

my $dbfile = "/now/affiliates/$AFFILIATE/content/override_mainstory.dbm";
my $OVERRIDEFILE = $dbfile;


print "Content-type: text/html\n\n";

# PART 1/2 -- setup $text
if (param('upload')) {
    my $fh = upload('myfile');
    if (!$fh && cgi_error) {
	$text = cgi_error();
    } 
    else {
	$text =  handle_upload();
    }
}

# PART 2/2  ----  always do this
&print_page($text); 

exit;

# from the current directory, figure out the affiliate
sub find_afl {

    my $afl = `pwd`;

	my $lasttoken = '';
    if ($afl  =~ m#.+/(\S+?)$#) {
		$lasttoken = $1;
	}
    return uc($lasttoken);
}

####---------------------------------------------------------------------------
# return $text
#
sub handle_upload {
    &log_msg("in handle upload....");

    open(LOG, ">>/tmp/nowadmin.log");
    select LOG;    $| = 1;
    select STDOUT; $| = 1;

    my $override = param('override');
    
    if (! $override) {
        undo_override($AFFILIATE, $OVERRIDEFILE);
	return "Override is off";
    } else {
        # code will create the .dbm file
    }

    my $keepimage = param('keepimage');

    if ($keepimage) {
       # do nothing
    } else {

        my $image =  param('myfile');

        if ($image) {
    	   $image    =~ s#.+\\##;

    	   ($image =~ m#^\.(.*)#g) 
           	&& (return "Sorry, but you cannot upload files that start with a . ");

           ($image !~ m#(jpg|JPG|gif|GIF|png|PNG)#go) 
                && (return "Sorry, but this file doesn't appear to be a .jpg/.gif/.png");

           ($image =~ m# #g) 
                && (return "Sorry, but spaces are not allowed in the filename ");

           save_image(1);
        } else {
	   save_image(-1);  # erase image
        }
    }   

    save_image(0);

        
    print <<__HTML__;
<HTML>
<HEAD>
<TITLE>News Over Wireless Administration</TITLE>
<link REL="stylesheet" TYPE="text/css" href="../style.css">
</HEAD>
<BODY BGCOLOR="#FFFFFF">

<TABLE BORDER="1" WIDTH="100%" CELLSPACING="0" CELLPADDING="5" BGCOLOR="#FFFFFF">
<TR>
<TD ALIGN="LEFT" VALIGN="MIDDLE" WIDTH=15%>
<IMG SRC="../images/logo-now.jpg"  BORDER=0>
</TD>
<TD ALIGN="CENTER" VALIGN="MIDDLE" WIDTH=100% BGCOLOR="#3399ff">
<span class="header">$AFFILIATE News Over Wireless Administration</span>
</TD>
</TR>
</TABLE><BR>
<STRONG><CENTER>Override is now in effect and will remain until turned off.<BR>
<a href="admin.cgi">Return to Main page</a>
</CENTER></STRONG>
__HTML__
exit;
}  


##---------------------------------------------------------------------------
## log_msg
##
sub log_msg
{
my ($msg) = @_;
my $datestamp = scalar(localtime());
my $fh = new FileHandle;
open($fh, ">>/tmp/mylog.txt");
print $fh "[$datestamp] $msg\n";
return();
}

##--------------------------------------------------------------
##   Prints out the page after all is done.
##
sub print_page  {
my ($final) = @_;
my $afl = $AFFILIATE;
print <<__HTML__;
<HTML>
<HEAD>
<TITLE>News Over Wireless Administration</TITLE>
<link REL="stylesheet" TYPE="text/css" href="../style.css">
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<TABLE BORDER="0" WIDTH="100%" CELLSPACING="0" CELLPADDING="5" BGCOLOR="#FFFFFF">
<TR>
<TD ALIGN="LEFT" VALIGN="MIDDLE" WIDTH=15%>
<IMG SRC="/images/logo-now.jpg"  BORDER=0>
</TD>
<TD ALIGN="CENTER" VALIGN="MIDDLE" WIDTH=100% BGCOLOR="#3399ff">
<span class="header">$afl News Over Wireless Administration</span>
</TD>
</TR>
</TABLE>
<DIV ALIGN=CENTER>
<STRONG>$final</STRONG></span>
<P>
<FORM ENCTYPE="multipart/form-data" METHOD=POST ACTION="admin.cgi">
<TABLE BORDER="0" CELLPADDING="4" CELLSPACING="2" align="center" width="700"> 
__HTML__
my $normal = 'CHECKED';
my $override = '';
my $head = '';
my $text = '';
my $imgname = '';
if (-e $OVERRIDEFILE) {
$normal = '';
$override = 'CHECKED'; 
my @v = read_db();
$head = $v[0];
$text = $v[1];
$imgname = $v[2];
(!$imgname) && ($imgname = '');
log_msg( " HEAD is $head");
}
#
# myfile
# mytext
# headline
#
print <<__HTML__;
<tr>
<td colspan="2">
<strong>Changing Your Top Story</strong>
<span class="help">
<ul>
<li>"Normal Operation" means your feed automatically updates the top story on the phone's main menu. </li>
<li>"Use Override Story" forces the top story to change more quickly in the case of breaking news.</li>
<li>The headline and body are required when issuing an override.</li>
<li>You may choose to include a breaking news image or not.</li>
<li>This tool does not affect your local news menu.</li>
<li>NOTE: Normal operation will not resume until you use this tool &amp; select "Normal Operation".</li>
</span>
</td>
</tr>
<tr>
<TD valign="top" width="100"><strong>Status:</strong></TD>
<TD>
<INPUT TYPE=RADIO NAME="override" VALUE="0" $normal>Normal Operation<br>
<INPUT TYPE=RADIO NAME="override" VALUE="1" $override>Use Override Story
</TD>
</TR>
<TR>
<TD COLSPAN=2><hr noshade size="1"></TD>
</TR>
<TR>
<TD><STRONG>Headline:</STRONG></td><td>(Required)</TD>
</tr>
<tr>
<TD ALIGN=LEFT colspan="2">
<INPUT NAME="headline" class="styleone" SIZE=60 VALUE="$head">
</TD>
</TR>
<TR>
<td><strong>Text:</strong></td><td>(Required)</td>
</tr>
<tr>
<td align=left colspan="2">
<textarea name="mytext" cols=58 rows=15>$text</textarea>
</TD>
</TR>
__HTML__
if ($imgname) {
print <<__HTML__;
<TR>
<TD ALIGN=CENTER COLSPAN=2>
Using image $imgname
</TD>
</TR>
<TR>
<TD ALIGN=CENTER COLSPAN=2>
<input type=checkbox name="keepimage" CHECKED>Continue using image $imgname
</TD>
</TR>
__HTML__
}
print <<__HTML__;
<TR>
<TD><strong>Image File:</strong></td><td>(Optional)</TD>
</tr>
<tr>
<TD ALIGN=LEFT colspan="2"><INPUT TYPE=FILE NAME="myfile" SIZE=45></TD>
</TR>
<TR>
<TD ALIGN=CENTER VALIGN=TOP COLSPAN=2>
<INPUT TYPE=SUBMIT NAME="upload" VALUE="Submit"> &nbsp; <INPUT TYPE=RESET VALUE="Start Over">
</TD>
</TR>
</TABLE>
</FORM>
<DIV><!--
<UL>
<LI><a href="video.cgi">Manage video files</a>
</UL> -->
</DIV>
</DIV>
</BODY>
</HTML>
__HTML__
}


####---------------------------------------------------------------------------
#### save_image
####
####  write the bytes directly into the dbm file
####---------------------------------------------------------------------------
sub save_image {


	my ($doimage) = @_;

	log_msg("in save_image dbfile = $dbfile\n");
	my %db;
	if (!tie(%db, "DB_File", $dbfile, O_RDWR | O_CREAT, 0664)) {
		log_msg("Could not tie to $dbfile");
	}
	if (-1 == $doimage) {
		delete $db{'img'};
		delete $db{'imgname'};
	}
	elsif ($doimage) {
		my $bytesread = 0;
		my $totalBytes = 0;
		# duh. this is a filehandle
		my $acc = '';
		my $buffer;
		my $fhsrc = upload('myfile');
		while ($bytesread = read($fhsrc, $buffer, 1024)) {
			$acc .= $buffer;
			$totalBytes += $bytesread;
		}
		$db{'img'} = $acc;   # store raw bytes
		my $uglyfile = param('myfile');

		# strip off the pathname
		my $idxa = rindex($uglyfile, "\\");
		if ($idxa > -1) {
			$uglyfile = substr($uglyfile, $idxa+1);
		}
		my $idxb = rindex($uglyfile, "\/");
		if ($idxb > -1) {
			$uglyfile = substr($uglyfile, $idxb+1);
		}
		$db{'imgname'} = $uglyfile;
	} else {
		$db{'head'} = param('headline');
		$db{'text'} = param('mytext');
		$db{'unixtime'}  = time();
		$db{'fresh'} = 1;                      # flag this as do-it-now
	}
	untie(%db);

}

#===============================================================
sub read_db() {
    my $fname;
    my $head;
    my $text;
    tie(my %db, "DB_File", $dbfile, O_RDWR | O_CREAT, 0664);
    $head =  $db{'head'};
    $text = $db{'text'};
    $fname = $db{'imgname'};
    untie(%db);
    return ($head, $text, $fname);
}


#===============================================================
# Bubbles now has direct access to /now  so just delete the file
#
sub undo_override {
    my ($afl, $override_file) = @_;

    unlink $override_file;
}
