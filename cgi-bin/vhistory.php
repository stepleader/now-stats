<?php 
    require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php');

	// get the affiliate from the present working directory
	$path      = getcwd();
	$pattern   = '/.*\/([^\/]+)$/';
	$replace   = '$1';
	$affiliate = strtoupper( preg_replace($pattern, $replace, $path) );

	// set up some useful date/time variables
	$date      = date("Y-m-d", time());
	$ystd      = date("Y-m-d", strtotime ("-1 day"));
	$today     = date("m/d/Y", time());

	// db connection should be abstracted out to an include
	$db        = "titania_db";
	$dbh       = mysql_connect("rdbase01", "nowapps", "n0wus3r");
	mysql_select_db($db, $dbh);

	$header = array ("Start", "Done", "Mins.", "Size (MB)", "Length (Mins.)", "Headline");

	if ($_GET) {
		if ($_GET['date']) {
			list($month, $day, $year) = split('[/.-]', $_GET['date']);
			if (checkdate($month, $day, $year)) {
				$date = $year . "-" . $month . "-" . $day;
				$day_sql = "AND start_time >= '$date 00:00' AND start_time <= '$date 23:59'";
			}
			else {
//				$day = "today";
				$day_sql = "AND start_time >= '$date 00:00'";
			}
		}
	}
	else if ($day == "yesterday") {
		$day_sql = "AND start_time >= '$ystd 00:00' AND start_time < '$date 00:00'";
	}
	else {
		$day_sql = "AND start_time >= '$date 00:00'";
	}
	$sql = "
SELECT 
TIME(submit_time), 
TIME(done_time), 
TIME_FORMAT(TIMEDIFF(done_time, submit_time), '%i:%s'), 
ROUND(filesize/1048576, 2), 
ROUND(duration/60, 1), 
headline 
FROM clips 
WHERE aflt='$affiliate' 
AND status='ok'
$day_sql
order by done_time DESC, submit_time DESC
";
	$result = mysql_query("$sql", $dbh);
	$total_encoded_clips = mysql_affected_rows();
	$total_encode_time = 0;
	$date_ts = MySQLtoTimestamp($date);
	$displaydate = date("m/d/Y", $date_ts);
	
function MySQLtoTimestamp($mysqlDate) {
    if (strlen($mysqlDate) > 10) {
        list($year, $month, $day_time) = explode('-', $mysqlDate);
        list($day, $time) = explode(" ", $day_time);
        list($hour, $minute, $second) = explode(":", $time);
        $ts = mktime($hour, $minute, $second, $month, $day, $year);
    } else {
        list($year, $month, $day) = explode('-', $mysqlDate);
        $ts = mktime(0, 0, 0, $month, $day, $year);
    }
    return $ts;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php echo "$affiliate Video"; ?></title>
<style type="text/css" title="text/css">
<?php include("/public/tools.now.com/webroot/includes/now.css"); ?>
</style>
</head>
<!-- <?php echo $sql; ?> -->
<body>
<div align="left">
<h1><?php echo $affiliate ?> Video</h1>
<h2><?php echo "$affiliate: $total_encoded_clips clips for $displaydate"; ?></h2>
<p>
<form name="daysform" method="get">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="left">Go To Date: <input type="text" name="date" size="10" value="<?php echo $displaydate; ?>"></td>
    <td align="left" valign="middle"><a href="javascript:show_calendar('daysform.date');"><img src="/images/show-calendar.gif" width="24" height="22" border="0"></a></td>
    <td align="left" valign="middle"><input type="submit" value="Go"></td>
  </tr>
</table>
</form>
</p>

<table>
  <tr id="err">
    <!-- th colspan="3">Encode Information</th -->
    <th colspan="3">Clip Information</th>
  </tr>

  <tr id="err">
<?php for ($x = 3; $x < count($header); $x++) { ?>
    <th><?php echo $header[$x]; ?></th>
<?php } ?>
  </tr>

<?php while ($row = mysql_fetch_row($result)) { ?>
	<?php $total_encode_time += $row[2]; ?>

  <tr id="err">
<?php for ($x = 3; $x < count($row); $x++) { ?>
	<?php $align = "right"; if ($x == 5) { $align = "left"; } ?>
    <td align="<?php echo $align; ?>"><?php echo $row[$x]; ?></td>
<?php } ?>
  </tr>

<?php } ?>
<?php $avg_encode_time = round ( ($total_encode_time / $total_encoded_clips), 2); ?>

</table>
<br />Total Clips: <?php echo $total_encoded_clips; ?>

<br />Avg. Encode Time: <?php echo $avg_encode_time; ?> minutes

</div>

<script language="javascript">
<!-- 
<?php include("/public/nowstats/includes/date-picker.js"); ?>
-->
</script>

</body>
</html>
