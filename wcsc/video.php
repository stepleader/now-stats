<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WCSC Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WCSC Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wcsc/app/">Premium App</a>
| <a href="/wcsc/iapp/">iPhone App</a>
| <a href="/wcsc/wap/">Mobile Web</a>
| <a href="/wcsc/sms/">SMS Usage</a>
| <a href="/wcsc/video.php">Video Views</a>
| <a href="/wcsc/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">77</td>
	<td align="right">0.27%</td>
	<td align="right">0</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">95</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">138</td>
	<td align="right">0.38%</td>
	<td align="right">0</td>
	<td align="right">24</td>
	<td align="right">0</td>
	<td align="right">162</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">125</td>
	<td align="right">0.33%</td>
	<td align="right">0</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">139</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">177</td>
	<td align="right">0.57%</td>
	<td align="right">1</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">194</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">106</td>
	<td align="right">0.32%</td>
	<td align="right">0</td>
	<td align="right">25</td>
	<td align="right">0</td>
	<td align="right">131</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">172</td>
	<td align="right">0.48%</td>
	<td align="right">2</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">183</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">333</td>
	<td align="right">0.48%</td>
	<td align="right">0</td>
	<td align="right">47</td>
	<td align="right">0</td>
	<td align="right">380</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">352</td>
	<td align="right">0.61%</td>
	<td align="right">1</td>
	<td align="right">30</td>
	<td align="right">0</td>
	<td align="right">383</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">287</td>
	<td align="right">0.74%</td>
	<td align="right">0</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">300</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">176</td>
	<td align="right">0.50%</td>
	<td align="right">1</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">178</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">230</td>
	<td align="right">0.63%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">230</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">277</td>
	<td align="right">0.82%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">278</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">257</td>
	<td align="right">0.76%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">259</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">175</td>
	<td align="right">0.57%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">176</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">293</td>
	<td align="right">0.87%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">293</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">160</td>
	<td align="right">0.55%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">160</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">180</td>
	<td align="right">0.68%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">184</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">86</td>
	<td align="right">0.28%</td>
	<td align="right">45</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">138</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">16</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
