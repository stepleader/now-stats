<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WTSP Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WTSP Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wtsp/app/">Premium App</a>
| <a href="/wtsp/iapp/">iPhone App</a>
| <a href="/wtsp/wap/">Mobile Web</a>
| <a href="/wtsp/sms/">SMS Usage</a>
| <a href="/wtsp/video.php">Video Views</a>
| <a href="/wtsp/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">71</td>
	<td align="right">0.24%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">72</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">104</td>
	<td align="right">0.28%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">106</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">136</td>
	<td align="right">0.36%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">136</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">120</td>
	<td align="right">0.39%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">120</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">153</td>
	<td align="right">0.46%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">158</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">114</td>
	<td align="right">0.32%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">115</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">355</td>
	<td align="right">0.51%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">361</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">963</td>
	<td align="right">1.68%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">997</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">490</td>
	<td align="right">1.26%</td>
	<td align="right">73</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">563</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">392</td>
	<td align="right">1.10%</td>
	<td align="right">39</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">431</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">421</td>
	<td align="right">1.16%</td>
	<td align="right">64</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">485</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">461</td>
	<td align="right">1.37%</td>
	<td align="right">76</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">537</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">481</td>
	<td align="right">1.43%</td>
	<td align="right">70</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">551</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">431</td>
	<td align="right">1.40%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">477</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">454</td>
	<td align="right">1.35%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">461</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">400</td>
	<td align="right">1.37%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">434</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">301</td>
	<td align="right">1.14%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">329</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">361</td>
	<td align="right">1.17%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">393</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">564</td>
	<td align="right">2.14%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">587</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">528</td>
	<td align="right">1.79%</td>
	<td align="right">58</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">586</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,099</td>
	<td align="right">1.80%</td>
	<td align="right">46</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,145</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">6,486</td>
	<td align="right">9.55%</td>
	<td align="right">48</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">6,534</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">816</td>
	<td align="right">2.32%</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">879</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">570</td>
	<td align="right">1.76%</td>
	<td align="right">62</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">632</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">634</td>
	<td align="right">1.97%</td>
	<td align="right">125</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">759</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">840</td>
	<td align="right">2.36%</td>
	<td align="right">131</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">971</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">893</td>
	<td align="right">2.49%</td>
	<td align="right">168</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,061</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">871</td>
	<td align="right">2.45%</td>
	<td align="right">99</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">970</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">491</td>
	<td align="right">2.00%</td>
	<td align="right">216</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">707</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">343</td>
	<td align="right">1.80%</td>
	<td align="right">213</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">556</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">457</td>
	<td align="right">2.25%</td>
	<td align="right">184</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">641</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">493</td>
	<td align="right">2.32%</td>
	<td align="right">273</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">766</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">557</td>
	<td align="right">3.42%</td>
	<td align="right">88</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">645</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">410</td>
	<td align="right">4.06%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">479</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">31</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">31</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
