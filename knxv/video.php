<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KNXV Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KNXV Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/knxv/app/">Premium App</a>
| <a href="/knxv/iapp/">iPhone App</a>
| <a href="/knxv/wap/">Mobile Web</a>
| <a href="/knxv/sms/">SMS Usage</a>
| <a href="/knxv/video.php">Video Views</a>
| <a href="/knxv/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">207</td>
	<td align="right">0.71%</td>
	<td align="right">86</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">293</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">307</td>
	<td align="right">0.84%</td>
	<td align="right">96</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">403</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">223</td>
	<td align="right">0.59%</td>
	<td align="right">85</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">308</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">162</td>
	<td align="right">0.52%</td>
	<td align="right">97</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">259</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">315</td>
	<td align="right">0.95%</td>
	<td align="right">54</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">369</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">448</td>
	<td align="right">1.25%</td>
	<td align="right">76</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">524</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">879</td>
	<td align="right">1.26%</td>
	<td align="right">49</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">928</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">190</td>
	<td align="right">0.33%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">197</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
