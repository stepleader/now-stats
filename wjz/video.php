<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WJZ Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WJZ Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wjz/app/">Premium App</a>
| <a href="/wjz/iapp/">iPhone App</a>
| <a href="/wjz/wap/">Mobile Web</a>
| <a href="/wjz/sms/">SMS Usage</a>
| <a href="/wjz/video.php">Video Views</a>
| <a href="/wjz/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">785</td>
	<td align="right">2.71%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">795</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">854</td>
	<td align="right">2.33%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">856</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">1,283</td>
	<td align="right">3.41%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,285</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">883</td>
	<td align="right">2.84%</td>
	<td align="right">78</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">961</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">1,344</td>
	<td align="right">4.05%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,377</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">741</td>
	<td align="right">2.07%</td>
	<td align="right">346</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,088</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">1,609</td>
	<td align="right">2.31%</td>
	<td align="right">247</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,856</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">1,328</td>
	<td align="right">2.32%</td>
	<td align="right">97</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,451</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1,466</td>
	<td align="right">3.75%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,515</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">771</td>
	<td align="right">2.17%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">806</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">954</td>
	<td align="right">2.62%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">965</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">996</td>
	<td align="right">2.96%</td>
	<td align="right">20</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,016</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">867</td>
	<td align="right">2.58%</td>
	<td align="right">44</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">911</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">904</td>
	<td align="right">2.93%</td>
	<td align="right">56</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">960</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">880</td>
	<td align="right">2.62%</td>
	<td align="right">72</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">973</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">712</td>
	<td align="right">2.44%</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">773</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">917</td>
	<td align="right">3.48%</td>
	<td align="right">85</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,154</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">882</td>
	<td align="right">2.86%</td>
	<td align="right">83</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,004</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">881</td>
	<td align="right">3.34%</td>
	<td align="right">87</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">968</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">1,385</td>
	<td align="right">4.69%</td>
	<td align="right">149</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,535</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">1,895</td>
	<td align="right">3.11%</td>
	<td align="right">202</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,097</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">2,339</td>
	<td align="right">3.45%</td>
	<td align="right">87</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2,426</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">1,313</td>
	<td align="right">3.74%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,382</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">1,083</td>
	<td align="right">3.34%</td>
	<td align="right">105</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,188</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">1,048</td>
	<td align="right">3.25%</td>
	<td align="right">149</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,197</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">1,197</td>
	<td align="right">3.36%</td>
	<td align="right">55</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,252</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">1,173</td>
	<td align="right">3.26%</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,226</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">1,093</td>
	<td align="right">3.07%</td>
	<td align="right">38</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,131</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">627</td>
	<td align="right">2.55%</td>
	<td align="right">94</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">721</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">447</td>
	<td align="right">2.35%</td>
	<td align="right">120</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">567</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">752</td>
	<td align="right">3.70%</td>
	<td align="right">211</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">963</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">750</td>
	<td align="right">3.53%</td>
	<td align="right">166</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">916</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">668</td>
	<td align="right">4.11%</td>
	<td align="right">184</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">852</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">347</td>
	<td align="right">3.43%</td>
	<td align="right">159</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">506</td>
</tr>
<tr>
	<td>May. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">77</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">77</td>
</tr>
<tr>
	<td>Apr. 2006</td>
	<td align="right">10</td>
	<td align="right">1.83%</td>
	<td align="right">66</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">76</td>
</tr>
<tr>
	<td>Mar. 2006</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">5</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
