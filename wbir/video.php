<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WBIR Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WBIR Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wbir/app/">Premium App</a>
| <a href="/wbir/iapp/">iPhone App</a>
| <a href="/wbir/wap/">Mobile Web</a>
| <a href="/wbir/sms/">SMS Usage</a>
| <a href="/wbir/video.php">Video Views</a>
| <a href="/wbir/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">36</td>
	<td align="right">0.10%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">36</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">231</td>
	<td align="right">0.33%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">231</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">113</td>
	<td align="right">0.20%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">113</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">1</td>
	<td align="right">0.00%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">183</td>
	<td align="right">0.50%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">184</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">152</td>
	<td align="right">0.45%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">155</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">140</td>
	<td align="right">0.42%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">145</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">167</td>
	<td align="right">0.54%</td>
	<td align="right">1</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">168</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">151</td>
	<td align="right">0.45%</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">156</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">107</td>
	<td align="right">0.37%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">109</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">104</td>
	<td align="right">0.39%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">107</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">207</td>
	<td align="right">0.67%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">214</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">292</td>
	<td align="right">1.11%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">300</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">145</td>
	<td align="right">0.49%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">156</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">426</td>
	<td align="right">0.70%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">442</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">252</td>
	<td align="right">0.37%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">260</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">222</td>
	<td align="right">0.63%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">231</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">156</td>
	<td align="right">0.48%</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">174</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">327</td>
	<td align="right">1.01%</td>
	<td align="right">15</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">342</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">289</td>
	<td align="right">0.81%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">305</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">471</td>
	<td align="right">1.31%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">504</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">540</td>
	<td align="right">1.52%</td>
	<td align="right">29</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">569</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">185</td>
	<td align="right">0.75%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">225</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">104</td>
	<td align="right">0.55%</td>
	<td align="right">66</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">170</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">13</td>
	<td align="right">0.06%</td>
	<td align="right">79</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">92</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
