<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WBMA Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WBMA Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wbma/app/">Premium App</a>
| <a href="/wbma/iapp/">iPhone App</a>
| <a href="/wbma/wap/">Mobile Web</a>
| <a href="/wbma/sms/">SMS Usage</a>
| <a href="/wbma/video.php">Video Views</a>
| <a href="/wbma/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">248</td>
	<td align="right">0.85%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">248</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">306</td>
	<td align="right">0.83%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">316</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">213</td>
	<td align="right">0.57%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">220</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">229</td>
	<td align="right">0.74%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">232</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">198</td>
	<td align="right">0.60%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">210</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">338</td>
	<td align="right">0.95%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">354</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">553</td>
	<td align="right">0.80%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">572</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">670</td>
	<td align="right">1.17%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">672</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">261</td>
	<td align="right">0.67%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">261</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">243</td>
	<td align="right">0.68%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">243</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">387</td>
	<td align="right">1.06%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">387</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">401</td>
	<td align="right">1.19%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">411</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">368</td>
	<td align="right">1.10%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">384</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">271</td>
	<td align="right">0.88%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">288</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">303</td>
	<td align="right">0.90%</td>
	<td align="right">6</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">309</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">209</td>
	<td align="right">0.72%</td>
	<td align="right">10</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">219</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">225</td>
	<td align="right">0.85%</td>
	<td align="right">14</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">239</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">257</td>
	<td align="right">0.83%</td>
	<td align="right">13</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">270</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">285</td>
	<td align="right">1.08%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">289</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">281</td>
	<td align="right">0.95%</td>
	<td align="right">9</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">290</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">477</td>
	<td align="right">0.78%</td>
	<td align="right">26</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">503</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">342</td>
	<td align="right">0.50%</td>
	<td align="right">12</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">354</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">230</td>
	<td align="right">0.66%</td>
	<td align="right">21</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">251</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">377</td>
	<td align="right">1.16%</td>
	<td align="right">61</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">438</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">380</td>
	<td align="right">1.18%</td>
	<td align="right">32</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">412</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">428</td>
	<td align="right">1.20%</td>
	<td align="right">17</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">445</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">385</td>
	<td align="right">1.07%</td>
	<td align="right">35</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">420</td>
</tr>
<tr>
	<td>Dec. 2006</td>
	<td align="right">365</td>
	<td align="right">1.03%</td>
	<td align="right">36</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">401</td>
</tr>
<tr>
	<td>Nov. 2006</td>
	<td align="right">231</td>
	<td align="right">0.94%</td>
	<td align="right">53</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">284</td>
</tr>
<tr>
	<td>Oct. 2006</td>
	<td align="right">203</td>
	<td align="right">1.07%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">243</td>
</tr>
<tr>
	<td>Sep. 2006</td>
	<td align="right">212</td>
	<td align="right">1.04%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">245</td>
</tr>
<tr>
	<td>Aug. 2006</td>
	<td align="right">321</td>
	<td align="right">1.51%</td>
	<td align="right">59</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">380</td>
</tr>
<tr>
	<td>Jul. 2006</td>
	<td align="right">219</td>
	<td align="right">1.35%</td>
	<td align="right">33</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">252</td>
</tr>
<tr>
	<td>Jun. 2006</td>
	<td align="right">199</td>
	<td align="right">1.97%</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">233</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
