<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WWBT Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>WWBT Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/wwbt/app/">Premium App</a>
| <a href="/wwbt/iapp/">iPhone App</a>
| <a href="/wwbt/wap/">Mobile Web</a>
| <a href="/wwbt/sms/">SMS Usage</a>
| <a href="/wwbt/video.php">Video Views</a>
| <a href="/wwbt/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">284</td>
	<td align="right">0.98%</td>
	<td align="right">20</td>
	<td align="right">63</td>
	<td align="right">0</td>
	<td align="right">367</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">333</td>
	<td align="right">0.91%</td>
	<td align="right">0</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">351</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">448</td>
	<td align="right">1.19%</td>
	<td align="right">1</td>
	<td align="right">18</td>
	<td align="right">0</td>
	<td align="right">467</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">227</td>
	<td align="right">0.73%</td>
	<td align="right">8</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">254</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">173</td>
	<td align="right">0.52%</td>
	<td align="right">5</td>
	<td align="right">27</td>
	<td align="right">0</td>
	<td align="right">205</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">194</td>
	<td align="right">0.54%</td>
	<td align="right">23</td>
	<td align="right">50</td>
	<td align="right">0</td>
	<td align="right">267</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">319</td>
	<td align="right">0.46%</td>
	<td align="right">29</td>
	<td align="right">34</td>
	<td align="right">0</td>
	<td align="right">382</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">376</td>
	<td align="right">0.66%</td>
	<td align="right">29</td>
	<td align="right">37</td>
	<td align="right">0</td>
	<td align="right">442</td>
</tr>
<tr>
	<td>Jul. 2008</td>
	<td align="right">266</td>
	<td align="right">0.68%</td>
	<td align="right">4</td>
	<td align="right">5</td>
	<td align="right">0</td>
	<td align="right">275</td>
</tr>
<tr>
	<td>Jun. 2008</td>
	<td align="right">324</td>
	<td align="right">0.91%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">328</td>
</tr>
<tr>
	<td>May. 2008</td>
	<td align="right">301</td>
	<td align="right">0.83%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">309</td>
</tr>
<tr>
	<td>Apr. 2008</td>
	<td align="right">733</td>
	<td align="right">2.18%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">750</td>
</tr>
<tr>
	<td>Mar. 2008</td>
	<td align="right">471</td>
	<td align="right">1.40%</td>
	<td align="right">8</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">479</td>
</tr>
<tr>
	<td>Feb. 2008</td>
	<td align="right">346</td>
	<td align="right">1.12%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">368</td>
</tr>
<tr>
	<td>Jan. 2008</td>
	<td align="right">457</td>
	<td align="right">1.36%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">480</td>
</tr>
<tr>
	<td>Dec. 2007</td>
	<td align="right">375</td>
	<td align="right">1.28%</td>
	<td align="right">3</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">378</td>
</tr>
<tr>
	<td>Nov. 2007</td>
	<td align="right">363</td>
	<td align="right">1.38%</td>
	<td align="right">28</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">399</td>
</tr>
<tr>
	<td>Oct. 2007</td>
	<td align="right">419</td>
	<td align="right">1.36%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">480</td>
</tr>
<tr>
	<td>Sep. 2007</td>
	<td align="right">419</td>
	<td align="right">1.59%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">430</td>
</tr>
<tr>
	<td>Aug. 2007</td>
	<td align="right">521</td>
	<td align="right">1.76%</td>
	<td align="right">41</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">562</td>
</tr>
<tr>
	<td>Jul. 2007</td>
	<td align="right">941</td>
	<td align="right">1.55%</td>
	<td align="right">74</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">1,015</td>
</tr>
<tr>
	<td>Jun. 2007</td>
	<td align="right">655</td>
	<td align="right">0.96%</td>
	<td align="right">69</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">724</td>
</tr>
<tr>
	<td>May. 2007</td>
	<td align="right">532</td>
	<td align="right">1.52%</td>
	<td align="right">16</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">548</td>
</tr>
<tr>
	<td>Apr. 2007</td>
	<td align="right">595</td>
	<td align="right">1.84%</td>
	<td align="right">40</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">635</td>
</tr>
<tr>
	<td>Mar. 2007</td>
	<td align="right">402</td>
	<td align="right">1.25%</td>
	<td align="right">59</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">461</td>
</tr>
<tr>
	<td>Feb. 2007</td>
	<td align="right">33</td>
	<td align="right">0.09%</td>
	<td align="right">57</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">90</td>
</tr>
<tr>
	<td>Jan. 2007</td>
	<td align="right">0</td>
	<td align="right">0.00%</td>
	<td align="right">2</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">2</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
