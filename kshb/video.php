<?php require_once(getenv('NOW_SW').'/core/lib/php/portal-auth.php'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KSHB Mobile Usage Stats</title>
<link rel="stylesheet" href="/includes/reset.css" type="text/css" /><link rel="stylesheet" href="/includes/base.css" type="text/css" /><link rel="stylesheet" href="/includes/stats.css" type="text/css" />

</head>
<body bgcolor="white">

<table class="page_header" border="0" cellpadding="2" cellspacing="1">
<tr>
<td class="page_header" align="left" valign="top" width="150">
<img src="/images/logo-now.jpg" border="0" alt="News Over Wireless" width="150" height="100">
</td>
<td class="page_header" align="left" valign="top">
<h2>KSHB Mobile Usage Statistics</h2>
<h3>Video Viewership</h3>
<p class="reportlinks">
<a href="/kshb/app/">Premium App</a>
| <a href="/kshb/iapp/">iPhone App</a>
| <a href="/kshb/wap/">Mobile Web</a>
| <a href="/kshb/sms/">SMS Usage</a>
| <a href="/kshb/video.php">Video Views</a>
| <a href="/kshb/video/vcast/">VCast Views</a>
<br><a href="http://www.newsoverwireless.com/help/stats.html" target="help">What does this mean?</a>

</p>
</td>
</tr>
</table>

<hr noshade size="1">
<div id="stats_report">

<table border="1" align="left" width="700">
<tr>
	<th rowspan="2" valign="bottom">
		Month
	</th>
	<th colspan="2">
		Sprint TV
	</th>
	<th colspan="1">
		Java App.
	</th>
	<th colspan="1">
		Mobile Web
	</th>
	<th colspan="1">
		Live
	</th>
	<th colspan="1">
		Total
	</th>
</tr>
<tr>
	<th>
		Views
	</th>
	<th>
		% of Total
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
	<th>
		Video Views
	</th>
</tr>
<tr>
	<td>Mar. 2009</td>
	<td align="right">158</td>
	<td align="right">0.54%</td>
	<td align="right">4</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">162</td>
</tr>
<tr>
	<td>Feb. 2009</td>
	<td align="right">197</td>
	<td align="right">0.54%</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">197</td>
</tr>
<tr>
	<td>Jan. 2009</td>
	<td align="right">288</td>
	<td align="right">0.77%</td>
	<td align="right">7</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">295</td>
</tr>
<tr>
	<td>Dec. 2008</td>
	<td align="right">203</td>
	<td align="right">0.65%</td>
	<td align="right">11</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">214</td>
</tr>
<tr>
	<td>Nov. 2008</td>
	<td align="right">165</td>
	<td align="right">0.50%</td>
	<td align="right">23</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">188</td>
</tr>
<tr>
	<td>Oct. 2008</td>
	<td align="right">357</td>
	<td align="right">1.00%</td>
	<td align="right">19</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">376</td>
</tr>
<tr>
	<td>Sep. 2008</td>
	<td align="right">634</td>
	<td align="right">0.91%</td>
	<td align="right">68</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">702</td>
</tr>
<tr>
	<td>Aug. 2008</td>
	<td align="right">145</td>
	<td align="right">0.25%</td>
	<td align="right">22</td>
	<td align="right">0</td>
	<td align="right">0</td>
	<td align="right">167</td>
</tr>
</table>
<br />


</div>

<br clear=all>

<hr noshade size="1">
<p class="footer">This report was generated Mar 26, 2009 at  4:45 PM EDT</p>
<p class="footer">
<strong>Disclaimer:</strong> Numbers on this report are not used for billing purposes.<br />
This report includes all traffic, including QA tools in use by some carriers and News Over Wireless.<br />
That traffic may not be included in your final remittance reports.
</p>

</body>
</html>
